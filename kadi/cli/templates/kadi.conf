<VirtualHost *:80>
    ServerName {{ server_name }}

    Redirect permanent / https://{{ server_name}}:443/
</VirtualHost>

<VirtualHost *:443>
    ServerName {{ server_name }}

    SSLEngine on
    SSLProtocol all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
    SSLHonorCipherOrder on
    SSLCipherSuite ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-CHACHA20-POLY1305
    SSLCertificateFile {{ cert_file }}
    SSLCertificateKeyFile {{ key_file }}
    {%- if chain_file %}
    SSLCertificateChainFile {{ chain_file }}
    {%- endif %}

    ProxyPassMatch ^/[^/]*\.(ico|png|xml|svg|webmanifest)$ !
    ProxyPass /static !
    ProxyPass / uwsgi://localhost:8080/

    Alias /static/ {{ static_path }}/
    Alias / {{ static_path }}/favicons/

    <Location /static>
        Header set Cache-Control public,max-age=31536000
        Header unset ETag
        Header unset Last-Modified
    </Location>

    <Directory {{ static_path }}>
        AllowOverride None
        Require all granted
    </Directory>

    XSendFile on
    XSendFilePath {{ storage_path }}
    XSendFilePath {{ misc_uploads_path }}

    ErrorDocument 404 /404
    ErrorDocument 500 "500 - The server encountered an internal error. Please try again later."

    LogFormat "%h %t \"%r\" %>s %B \"%{Referer}i\" \"%{User-agent}i\"" simple
    {% if anonip_bin -%}
    CustomLog "|{{ anonip_bin }} --output /var/log/apache2/access.log"
    {%- else -%}
    CustomLog /var/log/apache2/access.log
    {%- endif %} simple
    ErrorLog /var/log/apache2/error.log
    ErrorLogFormat "[%t] [%m] [%l] [pid %P] %F: %E: %M"
    LogLevel error
</VirtualHost>
