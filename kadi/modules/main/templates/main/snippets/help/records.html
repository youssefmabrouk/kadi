{# Copyright 2020 Karlsruhe Institute of Technology
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License. #}

<h4 id="records">
  <strong>{% trans %}Records{% endtrans %}</strong>
</h4>
<p>
  {% trans %}
    Records are the basic components of Kadi4Mat and can represent any kind of digital or digitalized object, e.g.
    arbitrary research data, samples, experimental devices or even individual processing steps.
  {% endtrans %}
  {% trans %}
    Records consist of metadata that can either stand alone or be linked to any number of corresponding data.
  {% endtrans %}
  {% trans %}They can also be grouped into collections or linked to other records, as described later.{% endtrans %}
</p>
<h5>{% trans %}Creating new records{% endtrans %}</h5>
<p>
  {% trans a_open='<a href="#templates"><strong>' | safe, a_close="</strong></a>" | safe %}
    Creating a new record first requires entering its metadata. This includes basic information, such as title, (unique)
    identifier and description. Additionally, generic extra metadata can be specified, specific for each different kind
    of record. This metadata consists of enhanced key-value pairs, where each entry has at least a unique key, a type
    and a corresponding value. Optionally, an additional term IRI (Internationalized Resource Identifier) and validation
    instructions can be specified. It is also possible to create templates for the generic metadata, as described in {{
    a_open }}Templates{{ a_close }}. The following values types can be used for this metadata:
  {% endtrans %}
</p>
<dl class="row mb-0">
  <dt class="col-xl-2"><strong>String</strong></dt>
  <dd class="col-xl-10">{% trans %}A single text value.{% endtrans %}</dd>
  <dt class="col-xl-2"><strong>Integer</strong></dt>
  <dd class="col-xl-10">
    {% trans %}A single integer value. Integer values can optionally have a unit further describing them.{% endtrans %}
  </dd>
  <dt class="col-xl-2"><strong>Float</strong></dt>
  <dd class="col-xl-10">
    {% trans %}
      A single floating point value. Float values can optionally have a unit further describing them.
    {% endtrans %}
  </dd>
  <dt class="col-xl-2"><strong>Boolean</strong></dt>
  <dd class="col-xl-10">
    {% trans em_open="<em>" | safe, em_close="</em>" | safe %}
      A single boolean value which can either be {{ em_open }}true{{ em_close }} or {{ em_open }}false{{ em_close }}.
    {% endtrans %}
  </dd>
  <dt class="col-xl-2"><strong>Date</strong></dt>
  <dd class="col-xl-10">{% trans %}A date and time value which can be selected from a date picker.{% endtrans %}</dd>
  <dt class="col-xl-2"><strong>Dictionary</strong></dt>
  <dd class="col-xl-10">
    {% trans %}A nested value which can be used to combine multiple metadata entries under a single key.{% endtrans %}
  </dd>
  <dt class="col-xl-2"><strong>List</strong></dt>
  <dd class="col-xl-10">
    {% trans %}
      A nested value which is similar to dictionaries with the difference that none of the values in a list have a key.
    {% endtrans %}
  </dd>
</dl>
<p>
  {% trans em_open="<em>" | safe, em_close="</em>" | safe %}
    Aside from the metadata, it is possible to set the visibility of a record to either {{ em_open }}private{{ em_close
    }} or {{ em_open }}public{{ em_close }}, the latter giving every logged-in user the ability to search for the record
    and view its contents without requiring explicit read permissions to do so. Finally, a record may directly be linked
    to other resources and its permissions may directly be specified, both of which aspects can also be managed after
    creating the record.
  {% endtrans %}
</p>
<p>
  {% trans %}
    Once the metadata of a record has been created, the actual data of the record can be added in a separate view, which
    by default the application will redirect to automatically. This is just one part of the various views to manage
    records, the next section describes the purpose of the others, which can be selected after going back to the record
    overview page.
  {% endtrans %}
</p>
<h5>{% trans %}Managing existing records{% endtrans %}</h5>
<p>
  {% trans %}
    On the record overview page, different views are available, each of which can be accessed through the respective tab
    in the navigation menu of a record. The individual tabs and their contents are briefly described below.
  {% endtrans %}
</p>
<dl class="row">
  <dt class="col-xl-2">{% trans %}Overview{% endtrans %}</dt>
  <dd class="col-xl-10">
    <p>
      {% trans a_open='<a href="#users"><strong>' | safe, a_close="</strong></a>" | safe %}
        This tab provides an overview of a record, mainly related to its metadata. Here, it is possible to edit or copy
        a record, if the corresponding permissions are fulfilled, where editing a record also allows deleting it. Note
        that this will move the record to the trash first, see also {{ a_open }}Users{{ a_close }}. Furthermore, records
        can be exported in various formats, published or favorited. Note that the publishing functionality is only
        available if at least one publication provider has been registered with the application.
      {% endtrans %}
    </p>
  </dd>
  <dt class="col-xl-2">{% trans %}Files{% endtrans %}</dt>
  <dd class="col-xl-10">
    <p>
      {% trans %}
        This tab provides an overview of the files associated with a record. Given the corresponding permissions, new
        files can be added, which is usually done by uploading locally stored files. However, certain types of files may
        also be created directly via the web interface. Existing files can either be downloaded in bulk or individually
        using the quick navigation menu of each file. Depending on permissions, this navigation also shows additional
        actions to quickly manage files.
      {% endtrans %}
    </p>
    {% trans %}
      Clicking on a file leads to a separate overview page of the corresponding file, which shows all additional file
      metadata. Furthermore, many file types include a built-in preview functionality. Here, it is also possible to edit
      a file's metadata or content, where editing the metadata also allows deleting the file. Some file types may offer
      direct editing of the actual file content, otherwise the regular upload functionality can be used.
    {% endtrans %}
  </dd>
  <dt class="col-xl-2">{% trans %}Links{% endtrans %}</dt>
  <dd class="col-xl-10">
    <p>
      {% trans %}
        This tab provides an overview of the resources a record is linked with, which includes other records as well as
        collections. Collections represent logical groupings of multiple records, while links between records can
        specify their relationship and also contain additional metadata. Furthermore, record links can be visualized in
        an interactive graph. Clicking on a record link leads to a separate view, which provides a more detailed
        overview of the link and the associated records.
      {% endtrans %}
    </p>
    {% trans %}
      Linking resources requires link permission in both resources that should be linked together. Note that users still
      won't be able to view any linked resources if they have no explicit permission to do so. However, a limited subset
      of information about record links (the ID of the linked record and the name and term IRI of the link) will always
      be shown as part of the record revisions.
    {% endtrans %}
  </dd>
  <dt class="col-xl-2">{% trans %}Permissions{% endtrans %}</dt>
  <dd class="col-xl-10">
    <p>
      {% trans em_open="<em>" | safe, em_close="</em>" | safe %}
        This tab provides an overview of the access permissions granted to individual users or groups of multiple users
        for a specific record. New permissions can be granted if the corresponding permissions to do so are fulfilled,
        which currently works by using predefined roles. Details about the specific permissions and actions each role
        provides can be found by clicking on the {{ em_open }}Roles{{ em_close }} popover.
      {% endtrans %}
    </p>
    {% trans %}
      Note that group roles are always shown to users being able to manage permissions, even if the group would normally
      not be visible. This way, existing group roles can always be changed and/or removed. Such group roles only contain
      very limited information about the group itself (its ID, title, identifier and visibility).
    {% endtrans %}
  </dd>
  <dt class="col-xl-2">{% trans %}Revisions{% endtrans %}</dt>
  <dd class="col-xl-10">
    {% trans em_open="<em>" | safe, em_close="</em>" | safe %}
      This tab provides a history of changes to a record's metadata, file metadata and links to other records. By
      clicking on {{ em_open }}View revision{{ em_close }} of a revision item, a separate view will open, providing a
      more detailed overview of the respective revision and corresponding changes.
    {% endtrans %}
  </dd>
</dl>
