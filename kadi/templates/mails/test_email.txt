{% trans %}Hello {{ displayname }}{% endtrans %},

{% trans server_name=config["SERVER_NAME"] %}
this is a test email from Kadi4Mat ({{ server_name }}) sent via the graphical sysadmin user interface.
{% endtrans %}
