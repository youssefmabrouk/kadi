# Release history

## 0.42.0 (Unreleased)

**Changes**

* Dropped support for Elasticsearch 7 and added support for Elasticsearch 8.
  Please see the relevant update notes in the documentation.

## 0.41.0 (2023-09-25)

**Additions**

* Added a new plugin hook `kadi_get_terms` to implement a search for term IRIs
  using an existing terminology service.
* Added a new first-party plugin `tib_ts` which integrates the term IRI search
  provided by the TIB terminology service.
* Added a preview for VTP files as used within VTK.

**Changes**

* Further improved the resource and generic record metadata searches when using
  very short queries.
* Extended the home page layout preferences with an additional setting to show
  only resources that the current user has explicit access permissions for.

## 0.40.0 (2023-08-29)

**Additions**

* Added a new submit field when creating records which allows to skip adding
  files directly after record creation.
* Added a new editor for creating record links more easily, which can also be
  used when creating new records.
* Added the ability to specify record links in "Record" templates.
* Added a new query parameter `explicit_permissions` to the search endpoints of
  records, collections and templates, which can be used via both the GUI and
  API to only return results that the current user has explicit access
  permissions for.

**Changes**

* Excluded additional information when exporting resources in different
  formats, including various user- and identity-related information as well as
  some only internally relevant attributes.
* Moved the `displayname` attribute of users from their identity to the user
  itself, which mainly affects the user information returned via the API as
  well as the JSON exports.
* Improved the encoding detection when previewing text-based files.

**Fixes**

* Fixed an incorrect validation of template data, allowing for incomplete
  permission data when creating or updating "Record" templates.

## 0.39.3 (2023-07-28)

**Changes**

* Added some additional weights to individual fields, such as titles and
  identifiers, to further improve the resource search results.

**Fixes**

* Relaxed the Content-Security-Policy again as it was causing issues with
  Shibboleth authentication in Chrome and possibly other browsers.

## 0.39.2 (2023-07-27)

**Fixes**

* Fixed broken validation of dynamic select form fields.

## 0.39.1 (2023-07-26)

**Changes**

* Attempting to change roles via the API that do not yet exist now returns
  status code `404` instead of `409`.
* Record templates may now also be selected in the generic metadata editor.
* Some only internally relevant user and identity attributes sent via the API
  and also used in the JSON exports have either been removed or made visible
  only to sysadmins.
* Information related to user's email addresses won't be included anymore when
  exporting resources.
* User information is now excluded by default when publishing resources.

**Fixes**

* Fixed resource group roles not being displayed correctly in some cases.

## 0.39.0 (2023-07-25)

**Additions**

* Added a configuration item `BROADCAST_MESSAGE_PUBLIC` to make the broadcast
  message visible to unauthenticated users as well, which is also configurable
  via the graphical sysadmin interface.

**Changes**

* Adjusted the appearance of the broadcast message to make it more visible.
* Added a toggle to the quick search to allow quickly retrieving resources via
  their persistent IDs.

**Fixes**

* Shortcuts to edit record links won't be shown to users anymore that have no
  permissions to edit them in the first place.
* Fixed the IRI validation of record link terms not being applied when
  specified via the API.

## 0.38.0 (2023-07-12)

**Additions**

* Added a shortcut for local user registration to the index page, if enabled.
* Added an interactive tour to get started with the basic and most important
  functionality.
* Added the possibility for users to specify an ORCID iD to be used when
  exporting or publishing resources.
* Added a fullscreen toggle to the Markdown editor.
* Added a toggle to hide labels in the interactive graph visualizations.

## 0.37.1 (2023-06-26)

**Fixes**

* Fixed the new version notification only working correctly on the home page.
* Fixed the default export data of RO-Crates still being included when
  explicitely deselecting all export types in the web interface.

## 0.37.0 (2023-06-26)

**Additions**

* Added a shortcut to directly edit record links via the respective overview
  page.
* Added basic system information to the graphical sysadmin interface.
* Added a multiline input to the `UserInputText` node of the experimental
  workflow editor.

**Changes**

* Adjusted the uWSGI configuration template. Please see the relevant update
  notes in the documentation.
* Added a filter to select the types of export data to be included in the
  RO-Crate export.
* Renamed the `kadi_register_blueprints` plugin hook to `kadi_get_blueprints`
  and removed its parameter. Returned blueprints will now be registered
  automatically by the application.
* Renamed the `kadi_register_capabilities` plugin hook to
  `kadi_get_capabilities`.
* Changed the Zenodo publication functionality to use RO-Crates for both
  records and collections in order to increase interoperability.
* Dropped support for Python 3.7 and added support for Python 3.11. Please see
  the relevant update notes in the documentation.
* Changed the record type value to a literal in the RDF export.
* Replaced the "What's new" section with a small notification in the help
  navigation item.
* Removed the record template selection in the record overview page, as
  templates can already be selected on the new record page in a unified manner.

**Fixes**

* Fixed slashes in file names potentially leading to invalid file bundles or
  exported RO-Crates.
* Fixed the titles of recently visited items sometimes being encoded
  incorrectly.

## 0.36.0 (2023-05-30)

**Additions**

* Added a toolbar button to the Markdown editor for toggling block quotations.
* Added additional API endpoints to directly retrieve files or record links via
  their ID.
* Added an interactive graph visualization for collections and their linked
  resources.

**Changes**

* The generic record metadata search now also supports fuzzy matching for key
  and string values in order to be more robust. Exact matches will still be
  prioritized in the search order, even when not specifying exact matches
  explicitely.
* Improved the styling of block quotation elements, which can appear in
  Markdown descriptions, for example.
* Removed the global `_actions` meta property when retrieving record files or
  uploads via the API.
* Adjusted the uWSGI configuration template. Please see the relevant update
  notes in the documentation.
* The input field to specify term IRIs of record links is now collapsed by
  default.
* Changed the content size of files to a string value in the RDF export.
* Removed the record link information in the metadata file of the RO-Crate
  export. This information may still be included in any exported JSON and RDF
  files.
* Flattened the structure of all nested entries in the metadata file of the
  RO-Crate export, namely author, license and publisher information.

**Fixes**

* Fixed the redirect URIs field not populating its values correctly in some
  cases when registering new OAuth2 applications.
* Fixed the ordering of search results not using the search relevance score
  when searching the extra metadata of records without an additional basic
  search query.
* Fixed record links still being retrievable via the API if a linked record is
  soft-deleted.

## 0.35.0 (2023-04-24)

**Additions**

* Added the built-in `UserInputSelect`, `UserInputForm`, `VariableList` and
  `VariableJson` nodes to the experimental workflow editor.
* Added a value output to the `UserInputChoose` node of the experimental
  workflow editor.

**Changes**

* Increased the maximum description length of all resources from 10.000 to
  50.000 characters.
* Parent collections are now shown above the collection overview page rather
  than in the "Links" tab.

**Fixes**

* Fixed search toggles not initializing their value correctly based on existing
  query parameters.
* Fixed saved searches incorrecly reporting unsaved changes. Existing searches
  might need to be saved again, as some of the query parameter handling related
  to the search filter functionality has been adapted in this context.
* Fixed the list of authorized OAuth2 applications appearing on the respective
  settings page not being limited to the current user.

## 0.34.0 (2023-04-01)

**Additions**

* Added a configuration reference as well as basic setup instructions for
  Shibboleth authentication to the documentation.
* Added previews for OBJ and XYZ files.
* Added a toggle to hide `null` values in the generic record metadata viewer.
* User identities retrieved via the API now include an `email_confirmed`
  property, which is only returned if the email address itself is returned.
* Added a configuration option to all existing authentication providers to
  require manual activation of newly registered user accounts.
* Added a first version of an RDF export for records and collections, which can
  also be used via the API.
* Added a separate export functionality for the generic record metadata, which
  can be used via the GUI or API. Currently, it only supports the `json` export
  type in different formats.

**Changes**

* Inactive users will now be redirected to a corresponding error page instead
  of being logged out automatically.
* Email addresses of users are now always visible to sysadmins via the GUI and
  API.
* The RO-Crate export now also includes the RDF export of each record.

**Fixes**

* Fixed boolean input node not being initialized correctly when loading an
  existing workflow in the experimental workflow editor.
* Added the missing `revisions` link for templates retrieved via the API.
* Fixed an incorrect content type potentially being returned when exporting
  resources.
* Fixed email contents being only partially translated.

## 0.33.0 (2023-02-27)

**Additions**

* Added an OAuth2 server implementation, which supports the authorization code
  grant type, the refresh token grant type and the revocation of existing
  tokens.
* Input fields used to specify unique identifiers now directly check for
  duplicates.
* Added a basic `sort` option for retrieving files of a record via the API,
  which is also usable via the GUI in the files overview page of a record.

**Changes**

* Personal access tokens now include a prefix to distinguish them with other
  types of access tokens. Note that tokens created previously will continue to
  work as is.
* Term IRIs specified in the generic metadata of records or as part of record
  links are now validated according to RFC3987. Note that existing IRIs are not
  updated automatically and might need to be adjusted manually.

**Fixes**

* Fixed undo/redo functionality not working when changing term IRI values in
  the generic record metadata editor.

## 0.32.0 (2023-02-01)

**Additions**

* Added functionality to save the current settings and filters in the search
  pages of different resources.
* Added functionality to specify a record template for collections that will be
  used as a default when adding new records to the corresponding collection via
  the GUI.
* Added a `ciphers` configuration setting for the LDAP authentication provider
  to specify custom SSL/TLS ciphers to use for secure connections.

**Changes**

* Revisions that are triggered due to a referenced resource being completely
  deleted will not be associated with a user anymore, e.g. when deleting a
  linked record. This is mainly to avoid confusion, as there is no user that
  can clearly be associated with these revisions.
* The `user` returned as part of revision objects via the API may now be `null`
  in cases like the one mentioned above.
* Linked records are now also shown in the "Links" tab of collections.

**Fixes**

* Fixed the range validation missing in the JSON Schema template export.
* Fixed the displayed revision type being wrong for the rare case of revisions
  being created while the corresponding resource is soft-deleted.

## 0.31.1 (2023-01-09)

**Changes**

* Hyperlinks in Markdown descriptions and configured navigation footer items
  are now always opened in a new tab.

**Fixes**

* Empty values are not clamped to a valid value anymore when changing the range
  validation of the generic record metadata editor.
* Fixed the preview of PDF files loading automatically regardless of file size.
* When completely deleting a record, its creator is now correctly used for any
  potential revisions concerning linked records.

## 0.31.0 (2022-12-17)

**Additions**

* Added dashboards to records as an experimental feature.
* Added a `range` validation to the generic record metadata to validate the
  range of numeric values.
* Added a new configuration key `WORKFLOW_FEATURES` to enable workflow-related
  functionality independent of experimental features.
* Added a small preview when linking records to help with determining the
  desired link direction.
* Added a new sidebar to show recently visited resources.

**Changes**

* Removed the `_meta.replaced_file` attribute included in responses when
  directly replacing files via the API.
* Ranges for numeric and date values specified in the generic record metadata
  search are now inclusive.
* Local usernames may now also contain single underscores in between
  alphanumeric characters.
* The quick search now also allows filtering by title.
* Made the generic record metadata validation options sortable via the GUI.

**Fixes**

* Fixed the layout of user and group images with heights much larger than their
  width.
* Fixed the MIME type of XML files sometimes being detected incorrectly.
* Fixed the exact match toggle for string values in the generic record metadata
  search.
* Fixed the ordering of tags in record templates sometimes being inconsistent.

## 0.30.1 (2022-11-29)

**Changes**

* The current depth value in the record link visualization is now also applied
  when navigating to another record via the respective hyperlink.
* The maximum width of images specified in Markdown descriptions is now limited
  to one third of the available width.

**Fixes**

* Fixed the creation or update of record templates via the API potentially
  leading to invalid template data being persisted. Note that affected
  templates that have been created between this and the previous release will
  not be migrated automatically. However, they can easily be fixed by updating
  or recreating them again via the API.

## 0.30.0 (2022-11-28)

**Additions**

* Added information about the globally configured user upload quota to the user
  statistics overview.
* Persistent IDs of record links and revisions are now shown (again) on their
  respective pages.
* Added a preview for HDF5 files.
* Added a more powerful role editor to manage permissions and members of all
  existing resource types.
* Added functionality to directly specify permissions while creating new
  templates.
* Added the ability to specify linked collections and permissions in "Record"
  templates.

**Changes**

* Adjusted the layout and functionality of the generic metadata editor to be
  more intuitive.
* When viewing diffs of individual revisions, only attributes with changes will
  be shown instead of all data.
* The `timestamp` and `user` attributes of base revisions are now included
  directly in each revision object when retrieving revisions via the API
  instead of in a nested `revision` object.
* User roles, group roles and role rules retrieved via the API will not include
  the permissions of each role anymore, as these are currently static anyways.
  To retrieve role permissions in general, the existing `/roles` endpoint may
  be used.
* Replaced the functionality to copy permissions when creating new records and
  collections with a more powerful role editor.

## 0.29.1 (2022-10-27)

**Additions**

* Added functionality to manage access on group level for databases configured
  within the InfluxDB plugin.

**Fixes**

* Fixed the old `MAX_UPLOAD_USER_QUOTA` configuration key not working anymore
  and the application falling back to the default quota instead.

## 0.29.0 (2022-10-25)

**Additions**

* Added a first version of an RO-Crate export for records and collections,
  which can also be used via the API.
* Added a new tab to the resource menu of the own user to get an overview of
  some basic statistics related to the created resources.
* Added functionality to specify term IRIs for individual generic record
  metadata entries and record links.
* Added a new submenu for editing existing record links.
* Added an additional menu to the top navigation bar to quickly navigate to
  various informational pages.
* Added functionality to specify global tokens for databases configured within
  the InfluxDB plugin.
* Added a separate preview for JSON files, which uses a hierarchical view
  instead of raw text.
* Added buttons to the full-text search interfaces to easily toggle exact
  matching for the given queries.

**Changes**

* Renamed the `MAX_UPLOAD_USER_QUOTA` configuration to just
  `UPLOAD_USER_QUOTA`. Note that the old configuration key will continue to
  work as is for the time being and is not being deprecated yet.
* PPTX and DOCX files are not previewed as archives anymore to avoid confusion.

**Fixes**

* Fixed the ordering of generic record metadata validation options sometimes
  being inconsistent.

## 0.28.1 (2022-10-02)

**Additions**

* Added some additional built-in nodes to the experimental workflow editor to
  specify notes, select bounding boxes and format strings.

**Fixes**

* Fixed a broken package combination preventing Kadi from working on new
  installations when using Python 3.7.

## 0.28.0 (2022-09-26)

**Additions**

* Added functionality to load "Extras" templates within nested values in the
  metadata editor.
* Newly created record links will now store a reference to their creator, which
  will also be included as `creator` in all relevant API endpoints. For
  existing record links, the creator of the outgoing record of each link will
  be used.
* Added a `revision` query parameter to all API endpoints for retrieving single
  resource revisions, which can be used to specify a particular revision to
  compare the current revision with.
* Added a new overview page for record links, which also includes information
  about whether there have been changes in the corresponding records since
  being linked.
* Improved the revision overview pages to make it easier to see all relevant
  changes. It is now also possible to toggle changes and to compare the current
  revision to the latest one.

**Changes**

* Removed the `about` property from the user schema, which is used by all API
  endpoints returning user information.

**Fixes**

* The regular MIME type and description of files are not replaced anymore when
  updating the content of existing files without explicitely specifying a new
  MIME type and/or description.

## 0.27.1 (2022-08-23)

**Fixes**

* Record link revisions are now updated directly if a linked record is
  (permanently) deleted.

## 0.27.0 (2022-08-23)

**Additions**

* Added a new configuration value `MAIL_SUBJECT_HEADER` to customize the
  subject field of mails sent via the application.
* Added functionality to specify Markdown descriptions for individual record
  files, either for existing ones via the GUI or API, or directly via the API
  endpoints used to upload new files.
* Added a new plugin hook `kadi_get_translations_bundles` to add custom
  translations for use in the frontend.
* Added a plugin for InfluxDB integration, enabling sysadmins to configure
  InfluxDB instances and users to query them through the API of Kadi.
* Added functionality to favorite resources, which are then shown on the home
  page.
* Added functionality to also create "Record" templates based on existing
  records in addition to "Extras" templates.
* Changes to record links are now tracked as part of the record revisions. Note
  that the revisions will still only require read permissions for the current
  record, however, only a very limited subset of information will be included
  in the revisions in regards to the record links.
* Added German translations for the integrated help page.

**Changes**

* The "Get started" section on the home page can now be toggled.
* Adding new list entries in the metadata editor will copy the structure of the
  last list entry, if applicable, instead of using the default string value.

**Fixes**

* Fixed some default values being omitted in JSON Schema template export.
* Timestamps of linked records are now also updated when removing or changing
  existing record links.

## 0.26.0 (2022-07-12)

**Additions**

* Added JSON and JSON Schema export functionality to templates, which can also
  be used via the API.
* Added a preferences menu to the settings page with functionality to change
  which kind of resources are shown on the home page.
* Added new API endpoints to restore and purge deleted templates according to
  the new soft-deletion functionality of templates.
* Added revision functionality to templates, including corresponding API
  endpoints to retrieve template revisions.
* Added a new overview page for templates supporting improved filter
  functionality and full-text search. The corresponding API endpoint to
  retrieve templates was adapted accordingly. Please see the relevant update
  notes in the documentation.
* Added functionality to search for exact phrases in the full-text search
  interfaces by using surrounding double quotes in the search query.
* Added a new plugin hook `kadi_get_preferences_config` to allow for
  plugin-specific configuration, which is shown in the preferences menu of each
  user if at least one corresponding plugin is in use.

**Changes**

* Previews for large Excel files are not loaded automatically anymore.
* Deleted templates are now first moved to the trash instead of being
  permanently deleted, similar to other resources.
* Replaced the `kadi_get_preview_scripts` plugin hook with a more generic
  `kadi_get_scripts` hook.
* Enforced legal notices now need to be accepted again when changing their
  content. Note that this only works when specifying them via the graphical
  sysadmin interface.
* Renamed the current (future stable) API version from `v1.0` to just `v1`.

**Fixes**

* Fixed potential crash when trying to create a template with a non-existent
  type via the GUI.

## 0.25.1 (2022-06-07)

**Fixes**

* Fixed potential crash when trying to fallback to the text-based preview for
  files.

## 0.25.0 (2022-06-07)

**Additions**

* Added a `user` query parameter to the full-text search interfaces and API
  endpoints of records, collections and groups to specify one or more user IDs
  to filter the resources by their creator.
* Added an additional navigation to the file overview pages to quickly switch
  to the previous/next file, if applicable.
* Added a preview for Excel files.

## 0.24.1 (2022-05-04)

**Fixes**

* Fixed keyboard shortcuts not being suppressed while using the simple text
  editor.

## 0.24.0 (2022-04-27)

**Additions**

* Added functionality to write mathematical equations rendered via KaTeX in all
  places where Markdown input is supported.
* Added some new configuration items to directly specify various legal notices,
  namely the terms of use (`TERMS_OF_USE`), a privacy policy (`PRIVACY_POLICY`)
  and a legal notice (`LEGAL_NOTICE`), which are also configurable via the
  graphical sysadmin interface.
* Added a new configuration item `ENFORCE_LEGALS` to force users to accept the
  terms of use and/or the privacy policy before proceeding, which is also
  configurable via the graphical sysadmin interface.
* Added a simple text editor to directly create and edit text-based files.
* Files can now be uploaded directly (without chunking) via the API up to a
  certain size, which was previously an experimental feature. The upload
  frontend selects a suitable upload type automatically based on the file size.
* Added a new plugin hook `kadi_get_licenses` to add custom licenses.

**Changes**

* Removed the `MAX_UPLOAD_SIZE` configuration item, as it did not add much
  value compared to the existing `MAX_UPLOAD_USER_QUOTA` configuration item.

**Fixes**

* Fixed CSV preview missing the first row if no header was detected.

## 0.23.1 (2022-03-23)

**Fixes**

* Fixed Celery wrapper command not working anymore for subcommands other than
  the Celery worker.

## 0.23.0 (2022-03-23)

**Additions**

* Added a basic sorting functionality to the CSV preview.

**Changes**

* Error responses related to insufficient access token scopes now include the
  required scopes in the `scopes` property.
* Moved the "Groups" navigation item next to "Users".
* The legend is now always included when exporting the record link
  visualization as SVG.
* Completely overhauled the navigation for managing different resources by
  removing the respective top navigation bars. The links that the old
  navigation bar offered were moved to the different navigation tabs, together
  with all other links and actions in a context-sensitive manner.
* Replaced the `kadi_get_resource_nav_items` plugin hook with a new hook
  `kadi_get_resource_overview_templates` to add additional contents to the
  overview pages of resources.

**Fixes**

* Fixed Celery workers sometimes not consuming tasks anymore after reconnecting
  with Redis.

## 0.22.0 (2022-02-16)

**Additions**

* Added a small "What's new" section to the homepage, which simply links to the
  changelog.
* Added a plugin hook `kadi_register_capabilities` to register capabilities of
  a Kadi instance, e.g. whether certain database extensions are installed.
* Added functionality to publish collections via Zenodo in addition to
  individual records.
* Added a new plugin hook `kadi_get_publication_form` to provide a custom form
  template to be used when publishing resources.
* Added additional customization options when publishing resources via the
  Zenodo integration.
* Added the option to save a previously selected Identity Provider when logging
  in via Shibboleth.

**Changes**

* Changed the bulk record file download functionality to directly return a ZIP
  archive as a stream, which can also be used via the API.
* Added a `file` parameter to the `kadi_get_preview_templates` plugin hook.
* Added a button to some preview components to force the preview to be loaded
  on demand for large files.
* Moved the "Trash" submenu from the "Settings" menu to the "Users" menu of the
  current user.
* Exported JSON and PDF record data now always excludes linked collections, as
  these usually do not contain relevant information currently.
* Exported JSON collection data now includes additional, customizable record
  data.
* Added a `resource` parameter to the `kadi_get_publication_providers` plugin
  hook.
* Renamed the `kadi_publish_record` hook to `kadi_publish_resource` and
  adjusted its parameters accordingly, including changes in relation to the new
  `kadi_get_publication_form` hook.

**Fixes**

* Fixed the file upload dropzone not reacting to selected files in some
  specific cases on some browsers.
* Record information is not included anymore when using the collection JSON
  export via the API if a scoped personal access token does not include the
  `record.read` scope.

## 0.21.1 (2022-01-13)

**Fixes**

* Fixed resumable uploads not loading correctly anymore.

## 0.21.0 (2022-01-13)

**Additions**

* Added two configuration items for customizing the index page, `INDEX_IMAGE`
  and `INDEX_TEXT`, which are also configurable via the graphical sysadmin
  interface.
* Added a plugin hook `kadi_get_index_templates` for more complex customization
  needs of the index page.
* Added a configuration item `ROBOTS_NOINDEX` to exclude a public Kadi instance
  from search indices, which is also configurable via the graphical sysadmin
  interface.
* Added a configuration item `BROADCAST_MESSAGE` to specify a global broadcast
  message, which is also configurable via the graphical sysadmin interface.
* Added a plugin hook `kadi_get_nav_footer_items` for more complex
  customization needs of the configurable navigation footer items.
* Added the functionality to send test emails via the graphical sysadmin
  interface.
* Added a plugin hook `kadi_get_about_templates` for customizing the about
  page.
* Added most of the missing German translations.

**Changes**

* Hyperlinks in the record link visualization now directly link to the
  visualization page of the respective record.
* Files of the current record are now listed first when selecting them in the
  Markdown editor, if applicable.
* Renamed the `FOOTER_NAV_ITEMS` configuration item to `NAV_FOOTER_ITEMS`.
  Please see the relevant update notes in the documentation.

**Fixes**

* Improved inconsistent scrolling behavior in Markdown editor on different
  browsers.
* Fixed range responses for files not working anymore, which are used e.g. when
  previewing multimedia files.

## 0.20.0 (2021-12-06)

**Additions**

* Added a flag to propagate the extra metadata filter to linked records as well
  when exporting records as JSON.

**Changes**

* It is not possible anymore to create duplicate record links. This constraint
  is not strictly enforced, so existing duplicate record links are not
  affected.
* Improved the login GUI to make the login process clearer when more than one
  authentication provider is enabled.
* Removed the possibility to use indices instead of keys in the extra metadata
  filter when exporting records via the API.

## 0.19.0 (2021-11-22)

**Additions**

* Added experimental API endpoint to directly upload a file.
* Added experimental plugin hook `kadi_get_storages` to register custom storage
  providers.
* Added an API endpoint to retrieve all tags.
* Added a new built-in user input node in experimental workflow editor.
* Added node execution profiles in experimental workflow editor.
* Added a new "Collaborator" role to records and collections, which grants
  "read" and "link" permissions.

**Changes**

* Changed the `_pagination.total_pages` property to always be at least `1`,
  even if the amount of total items is `0`.
* Return no search results when searching records via the API and all given
  collections to filter are invalid.
* Changed the way uWSGI is installed in the installation instructions and
  script. Please see the relevant update notes in the documentation.
* Dropped support for Python 3.6 and added support for Python 3.10. Please see
  the relevant update notes in the documentation.

**Fixes**

* Fixed some nodes not allowing multiple connections in output ports in
  experimental workflow editor.
* Fixed image preview not working for TIFF images using 16 bit pixels.

## 0.18.1 (2021-10-18)

**Fixes**

* Fixed all role rules being applied retroactively again when creating a new
  rule instead of only the newly created rule.

## 0.18.0 (2021-10-18)

**Additions**

* Added a `tag_operator` query parameter to the resource search interfaces and
  API endpoints to specify the operator that tags are filtered with.
* Added functionality to edit the names of existing record links via the GUI
  and API.
* Added functionality to select existing link names when linking records via
  the GUI.
* Added plugin hook `kadi_get_resource_nav_items` to add custom navigation
  items to the menus of records, files, collections, groups or templates in the
  GUI.
* Added functionality to automate the permission management of groups using
  rules with different conditions, initially limited to username conditions.
* Added a link direction filter to the record links graph visualization.

**Changes**

* Replaced the `hide_public` query parameter with `visibility` in all relevant
  resource search API endpoints.
* Made the visibility filters more powerful in all relevant resource search
  interfaces in accordance with the corresponding API changes.
* Renamed the `_actions.remove_link` property to `_actions.remove` when
  retrieving record links via the API.
* Added additional shortcuts to link existing or new records to the collection
  overview pages.
* Replaced the post record creation and update plugin hooks with a general post
  change hook `kadi_post_resource_change`.
* Renamed the values of the `direction` query parameter from `to` to `out` and
  from `from` to `in` when retrieving record links via the API.

**Fixes**

* Fixed the record link SVG export either producing incorrect results or not
  working at all when unicode symbols are involved.
* Special characters are now escaped automatically when specifying simple
  filter queries via the GUI or API.

## 0.17.0 (2021-09-13)

**Additions**

* Added customization options to exclude certain information in the JSON and
  PDF exports.
* Added API endpoints to export records and collections in all existing
  formats.
* Added plugin hooks for responding to record creations and updates.
* Added visibility to templates, functioning the same as for other resources.
* Added the ability to nest collections in simple hierarchies. Each collection
  may have multiple child collections and up to one parent collection.
* Added the ability to recursively retrieve records of nested collections via
  the web interface and API.
* Added an API endpoint to retrieve all possible resource roles.

**Changes**

* User information referring to a resource is now always excluded when using a
  scoped personal access token without `user.read` scope, even if the
  corresponding endpoint itself is usable.
* API endpoints expecting resource IDs in the JSON body now return status code
  `400` instead of `404` if the given resource does not exist.
* Made Elasticsearch optional for development installations.
* Dynamic selections for users and resources now exclude unnecessary results
  whenever possible, e.g. users that already have a role for a specific
  resource when managing permissions.
* Renamed and reorganized some menu items for file management.
* Trying to link a record with itself via the API now returns status code `409`
  instead of `400`.
* Added a custom dialog to the file upload interface to make overwriting
  multiple existing files easier.
* Made role names case insensitive when managing permissions via the API.

**Fixes**

* Fixed Markdown and CSV previews not working if a file's content was not
  detected as `text/plain`.

## 0.16.0 (2021-07-26)

**Additions**

* Added basic handling of potential multivalued user attributes retrieved via
  Shibboleth, configurable via the `multivalue_separator` configuration value.
* Added an additional button to the resource edit pages to save changes without
  leaving the edit page and added the submit buttons to the top as well.
* Added a separate section for update notes in the documentation.
* Added a direct selection of record files as links to all Markdown fields.
* Added a dismissable prompt for changing the language to the browser's
  preferred one, if possible.
* Added a CLI command to list all existing search indices.

**Changes**

* Improved the record PDF export to properly deal with long text cells without
  using truncation and many other related visual improvements.
* Improved the full-text search for records, collections and groups to give
  higher priority to exact matches.
* Adjusted the plugin specifications for using custom preview data components
  and added a new hook for custom scripts.
* Various smaller UI improvements.
* Changed the existing search mappings for records, collections and groups in
  order to further improve exact matches and searches with short queries.
  Please see the relevant update notes in the documentation.
* Refactored parsing of query parameter flags to allow specifying both typical
  truthy and falsy values and adjusted the API documentation accordingly.
* The button to download all record files is now shown for all records with at
  least one file.
* Updated some of the basic server configuration templates. Please see the
  relevant update notes in the documentation.
* Added additional metadata to record and collection JSON export.

**Fixes**

* Fixed corner case of being unable to package multiple files of a record if
  they are all empty.

## 0.15.1 (2021-06-28)

**Fixes**

* Fixed the direct upload of files (i.e. drawings and workflows) not working in
  certain cases.

## 0.15.0 (2021-06-25)

**Additions**

* Added a new filter parameter to the group search API and UI to retrieve only
  groups with membership.
* Added sections and information about record links and collections to the
  record PDF export.
* Added a toggle to the record links graph visualization to enable or disable
  forces.
* Added shortcuts to the metadata viewer for jumping directly to the respective
  entry in the editor.
* Added persistent IDs to the record PDF export
* Added an SVG export functionality to the record links graph.
* Added previews for BMP, TIFF and GIF image files.

**Changes**

* Changed the functionality to retrieve a user's groups in the API and UI to
  retrieve either created or common groups.
* Use existing archives if possible when downloading multiple record files as
  long as they are still up to date.
* Replaced the `psycopg2-binary` Python package with `psycopg2`. Please see the
  relevant update notes in the documentation.
* Pinned all Python dependencies to ensure reproducible installations.
* Changed all of the divided resource views (e.g. the record overview page) to
  only load content once a corresponding tab is actually shown.

**Fixes**

* Fixed inconsistent undo/redo behavior in the metadata editor.

## 0.14.0 (2021-06-07)

**Changes**

* Added success indicator when directly uploading drawings and workflows.
* Added indication for ports allowing multiple connections in experimental
  workflow editor.
* Added previews for some common multimedia formats.
* Show an overview of the currently used record template when creating new
  records.
* Added a direct selection of image record files to all Markdown fields.
* Added additional allowed socket combinations and the ability to view them
  when hovering over a socket in experimental workflow editor.
* Added a new configuration value `ELASTICSEARCH_CONFIG` to further customize
  the Elasticsearch connection for more intricate setups.
* Adjusted the Elasticsearch configuration in the installation script and
  manual installation instructions for the described single-node setup. Please
  see the relevant update notes in the documentation.
* Updated the structure of the documentation as well as some installation and
  configuration sections.
* Added support for environment tools in experimental workflow functionality.

**Fixes**

* Fixed rounding error in canvas painter potentially causing slightly blurry or
  misplaced images.
* Fixed broken scrolling behavior in experimental workflow editor for some
  OS/browser combinations.
* Replaced the broken `kadi utils secret-key` command with using `/dev/urandom`
  directly in the installation instructions and script.

## 0.13.0 (2021-05-20)

**Changes**

* Show current version in *About* page for authenticated users only.
* Adjusted priority of display name related attributes in LDAP configuration.
  If `firstname_attr` and `lastname_attr` are specified, they always take
  precedence over `displayname_attr`.
* Added a tabular preview for CSV files.
* Updated built-in user input nodes in experimental workflow editor.
* Included links to potential parent and child revisions in relevant API
  endpoints.
* Added a description field to templates.

**Fixes**

* Fixed preview for archives not working in some corner cases.
* Added missing metadata validation when creating record links via the API.
* Fixed search field in static selections also not receiving focus anymore.

## 0.12.0 (2021-04-23)

**Changes**

* Added a general `active_directory` flag for the LDAP authentication
  configuration, which also replaces the `supports_std_exop` flag.
* Improved the installation and configuration instructions in the
  documentation.
* Added a toggle to view the record metadata validation in the template
  overview.
* Excluded some data in the JSON export that is only relevant when using the
  API.
* Hide record template selection when copying records.

**Fixes**

* Fixed public shared user/group resources not appearing.
* Fixed LDAP authentication for Active Directories.
* Fixed some broken buttons when managing connected services or publishing
  records.
* Fixed preview for archives sometimes showing an incorrect folder structure.
* Fixed some buttons being visible without suitable permissions.
* Fixed search field in dynamic selections not receiving focus anymore.

## 0.11.0 (2021-04-14)

**Changes**

* Added a graph visualization for record links.
* Added some sections and revised many existing chapters in the documentation.
* Changed ordering of returned resources in record/collection link API
  endpoints.
* Updated built-in nodes in experimental workflow editor.

## 0.10.0 (2021-04-02)

**Changes**

* Updated some preview components and added a toggle to view the source text of
  Markdown files.
* Renamed "Copy" functionality to "Duplicate" in metadata editor.
* Added a preview for STL model files.

**Fixes**

* Fixed `utils` commands potentially using the wrong path value for virtual
  environments.

## 0.9.0 (2021-03-24)

**Changes**

* Updated built-in nodes in experimental workflow editor.
* Reintroduced basic keyboard shortcuts to easily switch to the different
  resource overview pages.
* Added the ability to specify optional validation instructions for the generic
  record metadata. This can be done both in templates and the actual records
  and is mainly intended for use in the graphical metadata editor to facilitate
  the manual entry of metadata. In the first version, `required` and `options`
  are supported.

**Fixes**

* Fixed crash in record PDF export related to `null` date values.
* Fixed some dependency ports not accepting multiple connections in
  experimental workflow editor.

## 0.8.1 (2021-03-17)

**Changes**

* Added `/info` endpoint to API.

**Fixes**

* Removed broken keyboard shortcuts again.

## 0.8.0 (2021-03-16)

**Changes**

* Added record template selection to "New record" menu as well.
* Added some basic keyboard shortcuts to switch to the different resource
  overview pages.
* Adjusted content shown on home page.

**Fixes**

* Explicitely pinned SQLAlchemy version to `<1.4.0`.

## 0.7.1 (2021-03-11)

**Fixes**

* Fixed crash in record PDF export related to parsing certain dates.

## 0.7.0 (2021-03-10)

**Changes**

* Improved the user management in the graphical sysadmin interface.
* Added the option to set the link direction when linking records.
* Added PDF export functionality to records.
* Major refactoring of experimental workflow editor and corresponding
  functionality.
* Added an option to reset the canvas size in the canvas painter.
* Improved help pages and added additional help popovers.
* Added additional filter functionality for resource permissions in the web
  interface and API.
* Major refactoring of record, collection and group search interfaces.
* Added collection filter to record search interface and API.

**Fixes**

* Hide "Update content" button in file overview if user lacks permission.
* Fixed broken filter functionality on some pages.
* Fixed permission checks for retrieving shared group resources via API.
* Fixed crash in extra metadata search in certain corner cases.

## 0.6.0 (2021-02-05)

**Changes**

* Added the first version of a graphical sysadmin interface.
* Various smaller UI improvements.

**Fixes**

* Made the canvas painter work with touch devices.
* Restricted extra metadata integer values to values safe for parsing them in a
  JS context, e.g. in the metadata editor.
* Fixed API endpoint to get a file of a record by its name.
* Fixed creating orphaned revisions after deleting resources.

## 0.5.0 (2021-01-13)

**Changes**

* Made endpoints to manage the trash/deleted resources part of the public API.
  These endpoints require a new scope for any personal access token
  (`misc.manage_trash`).
* Added plugin hooks for specifying custom MIME types, preview data and
  components.
* Added a preview for Markdown files.
* Added functionality to filter out public resources in the searches.
* Added the option to copy a template, to apply it from the overview and
  improved exporting extra record metadata as template.
* Added a "QR Code" export for records and collections.
* Added new tabs to the group overview to view resources shared with a group.
* Added a canvas painter to create drawings and upload them as files to a
  record.
* Added functionality to edit previous drawings and other images.
* Added many improvements to the documentation.
* Various other smaller improvements.

**Fixes**

* Fixed another bug in the latest migration script that caused it not to run
  under certain circumstances.
* Fixed and improved MIME type detection based on file's contents.
* Various other smaller bug fixes.

## 0.4.3 (2020-12-07)

* Fixed a bug in the latest migration script that caused it not to run under
  certain circumstances.

## 0.4.2 (2020-12-07)

* Unified record types to be always lowercase.
* Fixed Zenodo plugin for records with too short descriptions.

## 0.4.1 (2020-12-04)

* Improved some error messages.

## 0.4.0 (2020-12-03)

* Added functionality to publish records using different providers, which can
  be registered as plugins.
* Added a Zenodo plugin, enabling users to connect their accounts and to
  publish records.
* Added option to specify a common bind user to use for LDAP operations in the
  LDAP authentication provider.
* Added additional user management commands to the CLI.
* Various smaller bug fixes and GUI improvements.

## 0.3.0 (2020-11-10)

* Allow exact matches for keys and string values in the extra metadata search
  by using double quotes.
* Improved LDAP authentication and added the option to allow LDAP users to
  change their password.
* Slightly improved plugin infrastructure and added additional hooks.
* Added a new page to the settings page, allowing users to manage connected
  services, which can be registered as plugins.
* Added a license field to records.
* Various smaller bug fixes and improvements.

## 0.2.0 (2020-10-02)

* Removed linking resources with groups. Group links did not add much value in
  their current form but rather lead to confusion. Something similar might be
  brought back in the future again.
* Added and improved some more translations.
* Migrate Celery configuration to new format.
* Various smaller bug fixes and improvements.

## 0.1.2 (2020-09-22)

* Added an installation script for production environments and instructions.
* Some other updates to the documentation and configuration templates.

## 0.1.1 (2020-09-15)

* Cookies are now set only for the current domain.
* Small updates to the documentation and configuration templates.

## 0.1.0 (2020-09-14)

* Initial beta release.
