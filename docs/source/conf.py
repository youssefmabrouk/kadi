# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import sys
from importlib import metadata

import kadi.lib.constants as const
from kadi.lib.utils import utcnow


# -- System setup ------------------------------------------------------------

# Use the development environment when importing the application.
os.environ[const.VAR_ENV] = const.ENV_DEVELOPMENT

# Add the directory of our custom extensions to the search path so Sphinx can find them.
sys.path.append(os.path.abspath(".."))

# -- Project information -----------------------------------------------------

project = "Kadi4Mat"
copyright = f"{utcnow().year}, Karlsruhe Institute of Technology"
author = "Karlsruhe Institute of Technology"
release = metadata.version("kadi")

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be extensions coming
# with Sphinx (named "sphinx.ext.*") or your custom ones.
extensions = [
    "ext.autoflask",
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "myst_parser",
    "sphinx_inline_tabs",
    "sphinx_substitution_extensions",
    "sphinxcontrib.httpdomain",
]

# Configuration of the autodoc extension.
autodoc_default_options = {
    "exclude-members": "cache_ok",
    "member-order": "bysource",
    "show-inheritance": True,
}

# Change master doc from "contents" to "index".
master_doc = "index"

# Substitutions that will be available in every source file. Some of these are used as a
# workaround for dynamic content, since Sphinx does not support nested markup.
rst_prolog = f"""
.. |kadi| replace:: Kadi4Mat
.. |install-script| replace:: https://gitlab.com/iam-cms/kadi/-/raw/v{release}/bin/install_production.sh
.. |install-script-link| replace:: `here <https://gitlab.com/iam-cms/kadi/-/blob/v{release}/bin/install_production.sh>`__
"""

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.
html_theme = "sphinx_rtd_theme"

# Theme-specific options.
html_theme_options = {
    "logo_only": True,
    "navigation_depth": 5,
}

# Add any paths that contain custom static files (such as style sheets) here, relative
# to this directory. They are copied after the builtin static files, so a file named
# "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# Custom CSS files relative to the static path.
html_css_files = ["custom.css"]

# A logo to be placed at the top of the sidebar.
html_logo = "_images/kadi_rtd.svg"

# A custom favicon for the browser to use.
html_favicon = "_images/favicon.ico"
