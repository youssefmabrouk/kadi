.. _development-overview:

Project overview
================

This section is intended to give a brief overview about the general structure of
|kadi|'s source code. Specifically, the focus lies on everything contained inside the
``kadi`` directory.

.. _development-overview-assets:

assets
------

This directory contains all kinds of assets used in the application. Most assets are
used in the frontend, namely the JavaScript code, not including smaller scripts written
directly in some of the HTML templates, stylesheets and frontend translations, found in
the ``scripts``, ``styles`` and ``translations`` directories respectively.

Most code inside the ``scripts`` directory is written in plain JavaScript that is either
used globally or only on certain pages. All files using the ``.vue`` file extension
contain `Vue.js <https://vuejs.org>`__ components instead, which are used to encapsulate
reusable frontend code. See also :ref:`writing frontend code
<development-general-frontend-writing-code>`. The ``styles`` directory contains all
stylesheets used by the application, usually ``.scss`` files using the `Sass
<https://sass-lang.com>`__ extension. For more details about the translations, see
:ref:`Translations <development-translations-frontend>`.

.. _development-overview-cli:

cli
---

This directory is a Python package that contains code relevant for the Kadi
:ref:`command line interface <development-general-tools-kadi-cli>` (CLI). The main
command of the CLI is specified by using the ``console_scripts`` entry point, as defined
by the ``[project.scripts]`` section in the ``pyproject.toml`` file. For building the
actual interface, the `Click <https://click.palletsprojects.com>`__ library is used.

ext
---

This directory is a Python package that contains code relevant for all extensions used
in the application. Most of them are Flask extensions, which are specifically made to be
used inside an existing Flask application. They are often just wrappers over other
libraries, making the integration into a web application context easier. For most
extensions, a global extension instance is created that can be used throughout the
application.

lib
---

This directory is a Python package that contains code providing general or common
functionality as well as miscellaneous helper functions. See :ref:`Library <apiref-lib>`
for a complete API reference.

.. _development-overview-migrations:

migrations
----------

This directory contains all scripts and configuration files relevant for running
database schema migrations, which correspond to adjustments in the model classes defined
using `SQLAlchemy <https://www.sqlalchemy.org>`__. The ``versions`` directory contains
the actual, incremental migration scripts, ordered by their date of creation. Each
script has an upgrade and downgrade function, to apply or revert changes to a database
schema, and potentially data, respectively. See also how to
:ref:`adjust or add database models <development-general-backend-db-models>`.

modules
-------

This directory is a Python package that contains most of the main functionality
regarding the management of different resources, users, settings, etc. via the GUI and
the API. Each module (not to be confused with Python modules) is grouped into a separate
subdirectory, which structures all the related code and templates. See :ref:`Modules
<apiref-modules>` for a complete API reference.

plugins
-------

This directory is a Python package that contains code used for writing plugins, as well
as all first party plugins and internal implementations using the plugin functionality.
See :ref:`Plugins <apiref-plugins>` for a complete API reference.

.. _development-overview-static:

static
------

This directory contains static files that can be served, more or less, directly via a
web server, e.g. processed scripts and stylesheets or images. Regarding the former, see
also :ref:`writing frontend code <development-general-frontend-writing-code>`.

templates
---------

This directory contains common and globally used HTML templates, snippets and macros,
mostly making use of the `Jinja <https://jinja.palletsprojects.com>`__ templating
engine.

translations
------------

This directory contains different files needed for the backend translations. For more
details about the translations, see :ref:`Translations
<development-translations-backend>`.

vendor
------

This directory is a Python package that contains (adapted) third-party code.
