.. _installation-configuration-authentication:

Authentication
==============

This section explains all configuration related to user authentication.

.. data:: AUTH_PROVIDERS

    This configuration value specifies the authentication providers to be used for
    logging in to |kadi|. One or more providers can be specified as a list of
    dictionaries, where each dictionary needs to at least contain the type of the
    authentication provider. Please see the sections below for more details about each
    provider.

    Defaults to:

    .. code-block:: python3

        [
            {
                "type": "local",
            },
        ]

.. tip::
    :ref:`Sysadmins <installation-configuration-sysadmins>` can also manage various
    aspects regarding existing users via the graphical sysadmin interface, e.g.
    activating or deactivating individual user accounts.

Provider configuration
----------------------

Each authentication provider may contain various, provider-specific configuration
options that may need to be changed in order to make the provider work properly. The
following sections describe each provider and its options in more detail. Note that all
of the listed configuration values represent the default settings of the respective
provider.

Local
~~~~~

Accounts based on local authentication are managed by |kadi| itself. Therefore, this
option requires separate accounts to use |kadi|, but is also the easiest option to set
up and use. Local accounts can also change their own password and email address. Email
addresses of local accounts are not confirmed by default.

.. code-block:: python3

    {
        # The type of the authentication provider.
        "type": "local",
        # The title of the authentication provider that will be shown on the login page.
        "title": "Login with credentials",
        # The default system role of newly registered users. One of "admin", "member" or
        # "guest".
        "default_system_role": "member",
        # Whether newly registered users are automatically activated or require manual
        # activation by a sysadmin.
        "activate_users": True,
        # Whether to allow users that can access the website of Kadi4Mat to register
        # their own local accounts by specifying their desired username, display name,
        # email address and password.
        "allow_registration": False,
        # Whether email confirmation through the website of Kadi4Mat is required for
        # local accounts before any of its features can be used.
        "email_confirmation_required": False,
    }

If not allowing users to register their own accounts, local accounts can also be created
manually via the Kadi CLI after installing and configuring |kadi| successfully:

.. code-block:: bash

    sudo su - kadi    # Switch to the kadi user (in production environments)
    kadi users create # Interactively create a new user

.. tip::
    If local authentication is enabled, :ref:`sysadmins
    <installation-configuration-sysadmins>` can also create local users via the
    graphical sysadmin interface.

LDAP
~~~~

Accounts based on LDAP authentication are managed by a directory service implementing
the LDAP protocol. Therefore, this option allows one to use existing user accounts. LDAP
accounts cannot change their own email address, but may be able to change their password
through |kadi|. Email addresses of LDAP accounts are confirmed by default.

.. code-block:: python3

    {
        # The type of the authentication provider.
        "type": "ldap",
        # The title of the authentication provider that will be shown on the login page.
        "title": "Login with LDAP",
        # The default system role of newly registered users. One of "admin", "member" or
        # "guest".
        "default_system_role": "member",
        # Whether newly registered users are automatically activated or require manual
        # activation by a sysadmin.
        "activate_users": True,
        # Whether the LDAP server is an Active Directory.
        "active_directory": False,
        # The IP or hostname of the LDAP server.
        "host": "",
        # The port of the LDAP server to connect with. Besides the default port 389,
        # port 636 is generally used together with SSL/TLS.
        "port": 389,
        # The encryption method to use. Can be set to "ldaps" to use SSL/TLS for the
        # entire connection or "starttls" to use STARTTLS.
        "encryption": None,
        # Whether to validate the server's SSL/TLS certificate if an encryption method
        # is set.
        "validate_cert": True,
        # One or more SSL/TLS ciphers to use as a single string according to the OpenSSL
        # cipher list format (which separates multiple ciphers with colon) if an
        # encryption method is set. May also be set to "DEFAULT", in which case the
        # default ciphers of the installed OpenSSL version are used. Please be aware of
        # the possible security implications when changing this value.
        "ciphers": None,
        # The base DN where users are stored in the LDAP directory, which will be used
        # to perform a simple bind with a user for authentication and to retrieve the
        # additional user attributes after a successful bind. The former works
        # differently depending on whether the server is an Active Directory. For Active
        # Directories, a UserPrincipalName is constructed from the DN's domain
        # components in the form of "<username>@<domain>", where <username> is the
        # specified username when logging in. Otherwise, the full bind DN is constructed
        # as "<username_attr>=<username>,<users_dn>".
        "users_dn": "",
        # The full DN of a user that should be used to perform any LDAP operations after
        # a successful bind. By default, the bound (i.e. authenticated) user will be
        # used for these operations.
        "bind_user": None,
        # The password of the "bind_user".
        "bind_pw": None,
        # The LDAP attribute name to use for the (unique) username of the user, e.g.
        # "uid" or "sAMAccountName". Will also be used for the display name as fallback.
        "username_attr": "uid",
        # The LDAP attribute name to use for the email of the user.
        "email_attr": "mail",
        # The LDAP attribute name to use for the display name of the user. If
        # "firstname_attr" and "lastname_attr" have been specified, these two attributes
        # will take precedence.
        "displayname_attr": "displayName",
        # The LDAP attribute name to use for the first name of the display name instead
        # of "displayname_attr", e.g. "givenName". Must be used together with
        # "lastname_attr".
        "firstname_attr": None,
        # The LDAP attribute name to use for the last name of the display name instead
        # of "displayname_attr", e.g. "sn". Must be used together with "firstname_attr".
        "lastname_attr": None,
        # Whether to allow LDAP users to change their password via Kadi4Mat. This uses
        # the LDAP Password Modify Extended Operation to perform the password change, so
        # keep in mind that any password hashing is done server-side only.
        "allow_password_change": False,
        # Whether to send the old password when changing it, which might be required for
        # some LDAP servers.
        "send_old_password": False,
    }

.. note::
    If secure connections to the LDAP server fail, this may be a result of newer Python
    versions by default not enabling older SSL/TLS ciphers anymore that offer less
    security (e.g. no forward secrecy). To see which cipher is used by the server by
    default, the following command may be used to test the connection:

    .. code-block:: bash

        openssl s_client -connect <host>:<port>

    At the end of the command's output, details about the SSL/TLS session should be
    listed, including the corresponding SSL/TLS cipher that was used. This value may
    then be configured via the ``"ciphers"`` setting, as explained above.

.. _installation-configuration-authentication-shibboleth:

Shibboleth
~~~~~~~~~~

Accounts based on Shibboleth authentication are managed by one or more external
authentication providers, which may represent research institutions or other kinds of
organizations. Therefore, this option allows one to use existing user accounts.
Shibboleth accounts cannot change their own email address or password, and email
addresses of Shibboleth accounts are confirmed by default.

Conceptually, Shibboleth consists of a Service Provider (SP), in this case |kadi|, and
one or multiple Identity Providers (IdPs), which maintain and authenticate the user
accounts. An IdP may either be a single organization or a federation of multiple
organizations, such as the `DFN-AAI <https://doku.tid.dfn.de>`__ or the `SWITCHaai
<https://www.switch.ch/aai>`__, to facilitate the integration of multiple IdPs.

.. note::
    As setting up Shibboleth requires additional dependencies and configuration, please
    also refer to the :ref:`Shibboleth setup
    <installation-configuration-authentication-shibboleth-setup>` in addition to the
    |kadi| configuration explained below.

.. code-block:: python3

    {
        # The type of the authentication provider.
        "type": "shib",
        # The title of the authentication provider that will be shown on the login page.
        "title": "Login with Shibboleth",
        # The default system role of newly registered users. One of "admin", "member" or
        # "guest".
        "default_system_role": "member",
        # Whether newly registered users are automatically activated or require manual
        # activation by a sysadmin.
        "activate_users": True,
        # A list of valid IdPs. Each IdP has to be specified as a dictionary containing
        # the IdP's entity ID ("entity_id") and display name ("name"), e.g.:
        # [
        #    {"name": "Example Institute", "entity_id": "https://example.com/idp/shibboleth"},
        #    {"name": "ACME Corp.", "entity_id": "https://idp.acme.corp/shibboleth"},
        # ]
        "idps": [],
        # The encoding of the environment variables set via the Apache Shibboleth module
        # containing the user and meta attributes.
        "env_encoding": "latin-1",
        # The separator to use for splitting attributes containing multiple values.
        "multivalue_separator": ";",
        # The entityID of the SP. Defaults to "<url_scheme>://<server_name>/shibboleth",
        # where <url_schema> is either "http" or "https" and <server_name> corresponds
        # to the configured "SERVER_NAME" Kadi4Mat system setting.
        "sp_entity_id": None,
        # The relative path of the session (login) initiator of the SP.
        "sp_session_initiator": "/Shibboleth.sso/Login",
        # The relative path of the (local) logout initiator of the SP.
        "sp_logout_initiator": "/Shibboleth.sso/Logout",
        # The Shibboleth meta attribute name to extract the entityID of the current IdP
        # from.
        "idp_entity_id_attr": "Shib-Identity-Provider",
        # The Shibboleth meta attribute name to extract the display name of the current
        # IdP from.
        "idp_displayname_attr": "Meta-displayName",
        # The Shibboleth meta attribute name to extract the support email address of the
        # current IdP from.
        "idp_support_contact_attr": "Meta-supportContact",
        # The Shibboleth attribute name to use for the (unique) username of the user,
        # which should correspond to the attribute used to extract the
        # "eduPersonPrincipalName". This name specifies a (generally) human-friendly
        # username, which is combined with an IdP-specific scope value in the form of
        # "<username>@<scope>", making it unique. Will also be used for the display name
        # as fallback.
        "username_attr": "eppn",
        # The Shibboleth attribute name to use for the email of the user.
        "email_attr": "mail",
        # The Shibboleth attribute name to use for the display name of the user.
        "displayname_attr": "displayName",
        # The Shibboleth attribute name to use for the first name. Only used if no
        # display name could be found, in combination with "lastname_attr".
        "firstname_attr": "givenName",
        # The Shibboleth attribute name to use for the last name. Only used if no
        # display name could be found, in combination with "firstname_attr".
        "lastname_attr": "sn",
    }

.. _installation-configuration-authentication-shibboleth-setup:

Setting up Shibboleth
---------------------

Shibboleth has currently been tested with the official Shibboleth **Service Provider 3**
in combination with the Apache web server using the **mod_shib** module, which the
following instructions are also based on. As configuring Shibboleth correctly heavily
depends on the specific IdP(s) that should be used, the following sections attempt to
provide the most important general and |kadi|-specific instructions to make Shibboleth
authentication work.

Obtaining a certificate
~~~~~~~~~~~~~~~~~~~~~~~

The SP requires a certificate in order to sign authentication requests and to decrypt
SAML assertions. Usually, this certificate may be self-signed and differs from the
server certificate that |kadi| already uses for HTTPS. The requirements for this
certificate and how long it may be valid differs per IdP. A good starting point to
generate a certificate is the documentation of the `SWITCHaai
<https://www.switch.ch/aai/guides/sp/configuration/#4>`__.

Once generated, the certificate and key files should be moved to a suitable place on the
machine where |kadi| is installed and given suitable permissions:

.. code-block:: bash

    sudo mv <cert_file> /etc/ssl/certs
    sudo chmod 644 /etc/ssl/certs/<cert_file>
    sudo chown root:root /etc/ssl/certs/<cert_file>

    sudo mv <key_file> /etc/ssl/private
    sudo chmod 640 /etc/ssl/private/<key_file>
    sudo chown root:ssl-cert /etc/ssl/private/<key_file>

.. note::
    Independent of how the certificate is obtained, once it is expired, a `certificate
    rollover <https://www.switch.ch/aai/guides/sp/certificate-rollover>`__ has to be
    performed.

Installing additional dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Shibboleth SP and the corresponding Apache module have to be installed and enabled
by running:

.. code-block:: bash

    sudo apt install libapache2-mod-shib
    sudo a2enmod shib

During the installation, a new user *_shibd* will be created automatically. This user
needs access to the certificate used by Shibboleth later on, so it should be added to
the *ssl-cert* group:

.. code-block:: bash

    sudo usermod -a -G ssl-cert _shibd

Configuring Shibboleth
~~~~~~~~~~~~~~~~~~~~~~

In order to configure Shibboleth, the main configuration file at
``/etc/shibboleth/shibboleth2.xml`` needs to be adjusted. Before doing so, it might be a
good idea to create a backup of it. Generally, some of this configuration may depend on
the specific IdPs that should be configured, the following sections simply list the most
important XML elements that require changes:

* ``<ApplicationDefaults>``: This element specifies the general behavior of the SP. The
  following two attributes need to be specified or changed:

    * ``entityID``: The unique identifier of the SP, which should match the value
      configured via the ``"sp_entity_id"`` |kadi| setting.
    * ``metadataAttributePrefix``: Should be set to ``Meta-``, so |kadi| can extract
      various Shibboleth meta attributes without changing its default configuration.

* ``<Sessions>``: This element configures how SSO sessions should be handled by the SP.
  While not required by |kadi|, it is highly recommended to change the value of the
  ``redirectLimit`` attribute to ``exact``, as the default value leaves the SP
  vulnerable to open redirect attacks.
* ``<SSO>``: This element specifies the configured single sign-on procotols within the
  SP and generally depends on the IdPs that should be used. However, as |kadi| already
  provides a selection ("discovery") of IdPs on its login page, configured via the
  ``"idps"`` |kadi| setting, no attributes should be necessary here. The content of the
  element should be set to ``SAML 2``.
* ``<Logout>``: This element specifies the configured logout protocols for the SP. As
  |kadi| only supports "local" logout at the moment (i.e. logout will only be performed
  for the current SP), the content of the element should be set to ``Local``.
* ``<MetadataProvider>``: This element specifies how metadata are exchanged between the
  SP and the IdPs and therefore heavily depends on the IdPs that should be used. As
  usually metadata are distributed using XML files, a basic example of specifying a
  metadata provider (which may contain metadata for one or multiple IdPs) may look like
  the following:

  .. code-block:: xml

    <MetadataProvider type="XML"
                      url="https://example.com/idp-medatata.xml"
                      backingFilePath="idp-medatata.xml"
                      reloadInterval="3600"/>

* ``<CredentialResolver>``: This element specifies the certificate and key files to use
  for the SP. Here, the corresponding files obtained from the first step have to be
  configured, e.g.:

  .. code-block:: xml

    <CredentialResolver type="Chaining">
        <!-- Active certificate. -->
        <CredentialResolver type="File"
                            key="/etc/ssl/private/<key_file>"
                            certificate="/etc/ssl/certs/<cert_file>"/>

        <!-- Additional certificates can also be configured, e.g. when performing a certificate rollover. -->
    </CredentialResolver>

Once Shibboleth itself is configured, the attribute map file at
``/etc/shibboleth/attribute-map.xml`` needs to be adjusted as well. This file specifies
the attributes that Shibboleth should attempt to extract based on the information
retrieved from an IdP. Usually, the default map should mostly work already, however, the
following LDAP-based attributes might need to be enabled (or added, if not already
present) in addition:

.. code-block:: xml

    <Attribute name="urn:oid:2.5.4.3" id="cn"/>
    <Attribute name="urn:oid:2.5.4.4" id="sn"/>
    <Attribute name="urn:oid:2.5.4.42" id="givenName"/>
    <Attribute name="urn:oid:2.16.840.1.113730.3.1.241" id="displayName"/>
    <Attribute name="urn:oid:0.9.2342.19200300.100.1.3" id="mail"/>

Finally, the Shibboleth daemon has to be restarted to pick up all changes:

.. code-block:: bash

    sudo systemctl restart shibd

Afterwards, it is recommended to take a look at the corresponding log file at
``/var/log/shibboleth/shibd.log`` to identify any potential configuration errors.

Configuring Apache
~~~~~~~~~~~~~~~~~~

In order to configure Shibboleth in the Apache web server, the corresponding
configuration file has to be adjusted, e.g. ``/etc/apache2/sites-available/kadi.conf``
when using the structure as described in the installation instructions:

.. code-block:: apache

    # Before "ProxyPass / uwsgi:..."
    ProxyPass /Shibboleth.sso !

    # After "<Location /static>...</Location>"
    <Location />
        AuthType shibboleth
        Require shibboleth
    </Location>

This configuration ensures that all requests to ``/Shibboleth.sso/*`` will be handled by
Shibboleth (assuming this prefix was not changed via any other Shibboleth or |kadi|
setting), while the directive below defines a global rule to enable Shibboleth on all
pages. This way, Shibboleth will generally be active, but won't block access to any
pages (as this will be handled by |kadi| directly).

Configuring Kadi4Mat
~~~~~~~~~~~~~~~~~~~~

Once all external dependencies are installed and configured, all that is left is to
configure |kadi| :ref:`itself <installation-configuration-authentication-shibboleth>`.

.. note::
    Even if configured successfully, authentication might not work out-of-the-box, as
    individual SPs might need to be enabled manually in each IdP that should be used by
    the corresponding IdP administrators. Some IdPs might also not release all required
    attributes to |kadi| by default. In this case, users will be presented with a
    corresponding error page when attempting to log in to |kadi|.
