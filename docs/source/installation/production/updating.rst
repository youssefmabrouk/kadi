.. _installation-production-updating:

Updating the application
========================

This section describes how to update an existing |kadi| installation. It is highly
recommended to always keep a productive |kadi| installation up to date.

Preparations
------------

.. warning::
    It is generally recommended to create an up-to-date :ref:`backup
    <installation-production-backup>` of at least the database before updating the
    application.

**Before performing the update**, it is highly recommended to take a look at the
:ref:`update notes <installation-production-updating-notes>` below to be informed about
any new installation requirements or other changes between versions. The current version
that a |kadi| installation uses can be retrieved in various ways:

* Via the *Information* tab in the graphical :ref:`sysadmin
  <installation-configuration-sysadmins>` interface, which will also show whether the
  current version is up to date.
* Via the web interface at the bottom left of the *About* page when being logged in.
* Via the :ref:`HTTP API <httpapi>` using the ``/api/info`` endpoint.
* Via the Kadi CLI directly on the server:

    .. code-block:: bash

        sudo su - kadi # Switch to the kadi user
        kadi --version # Get the current Kadi version

Please also check whether any third party :ref:`plugins <installation-plugins>` that may
be used are still compatible with the new |kadi| version to be installed, as plugin hook
specifications and APIs of |kadi| that a plugin might use are subject to change.

.. _installation-production-updating-performing:

Performing the update
---------------------

When updating |kadi|, it is recommended to first stop the application and some of its
related services:

.. code-block:: bash

    sudo systemctl stop apache2 kadi-uwsgi kadi-celery kadi-celerybeat

Afterwards, the application can be updated:

.. code-block:: bash

    sudo su - kadi      # Switch to the kadi user
    pip install -U pip  # Make sure the newest version of pip is being used
    pip install -U kadi # Update the application code
    kadi db upgrade     # Upgrade the database schema

.. warning::
    Make sure to actually run the database schema upgrade and that it runs through
    successfully before continuing.

Finally, the application and services can be started again:

.. code-block:: bash

    sudo systemctl start apache2 kadi-uwsgi kadi-celery kadi-celerybeat

(Optional) Updating dependencies
--------------------------------

When (also) updating any dependencies |kadi| relies on, additional update steps may be
required. Generally, please refer to the documentation of the respective dependency and
make sure that the application has been backed up. Depending on which kind of dependency
should be updated, it might be necessary to stop the application beforehand, as
explained above.

For updating the used PostgreSQL version in particular, it is usually recommended to
migrate existing data using the same procedure as when handling :ref:`database backups
<installation-production-backup-database>`. For updating the used Python version, please
refer to the instructions about :ref:`using different Python versions
<installation-production-python-updating>`, as the described update steps should be the
same even when not compiling Python from source.

.. _installation-production-updating-notes:

Update notes
------------

The following section lists all changes, and corresponding versions, that may require
additional pre- or post-installation steps for existing installations. For all steps
that are to be performed **after** updating, it is usually still recommended to perform
them before starting all services again.

.. tip::
    For a full list of changes, please also refer to the :ref:`release history
    <release-history>`.

.. note::
    When updating from very old versions of |kadi|, it should usually be sufficient
    simply going through all changes step-by-step, starting with the oldest version that
    is relevant.

Version 0.42.0
~~~~~~~~~~~~~~

* **Before and during the update:** The supported version of Elasticsearch has been
  changed from version **7** to version **8**, so it has to be upgraded accordingly in
  existing installations. Before doing so, it is highly recommended to upgrade to the
  most recent minor version, or at least version **>=7.17**. The following example shows
  how to retrieve the current version, assuming Elasticsearch is reachable locally on
  the default port:

  .. code-block:: bash

    curl http://localhost:9200

  Afterwards, the upgrade to the next major version can be performing by using the
  following steps, which assume a simple, single-node setup as used in the installation
  instructions and script. It is recommended to stop the application beforehand, so no
  changes in the search index will be attempted in the meantime.

  .. code-block:: bash

    sudo systemctl stop elasticsearch
    echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
    sudo apt update && sudo apt upgrade
    sudo systemctl start elasticsearch

  .. note::
    If being prompted about conflicting changes in the Elasticsearch config file, make
    sure to keep the existing file, so previous configuration changes are not lost.

  Afterwards, the existing search index should work as before, without the need to
  reindex any data. The old Elasticsearch 7 package repository in
  ``/etc/apt/sources.list.d`` may optionally be deleted.

Version 0.37.0
~~~~~~~~~~~~~~

* **Before the update:** The uWSGI configuration template has been adapted again. While
  not required, it is recommended to update it when using a configuration such as
  described in the installation instructions or as generated by the installation script.
  The following line may be added to ``/etc/kadi-uwsgi.ini``:

  .. code-block::

    socket-timeout = 600

  This value ensures a sufficiently high timeout between application server and web
  server.
* **Before the update:** Support for Python version **3.7** has been removed. For
  existing installations that use this version, please see the instructions on how to
  install and use a :ref:`different Python version <installation-production-python>`.

Version 0.36.0
~~~~~~~~~~~~~~

* **Before the update:** The uWSGI configuration template has been adapted. While not
  required, it is recommended to update it when using a configuration such as described
  in the installation instructions or as generated by the installation script. The
  following lines may be changed in/added to ``/etc/kadi-uwsgi.ini``:

  .. code-block::

    harakiri = 3600
    max-worker-lifetime-delta = 5

  These values ensure that the application allows for longer request timeouts and that
  workers won't all restart at the same time when their maximum lifetime is reached.

Version 0.26.0
~~~~~~~~~~~~~~

* **After the update:** Full-text search functionality has been added for templates. In
  order to create the corresponding search index and to add existing templates to it,
  the Kadi CLI can be used:

  .. code-block:: bash

    sudo su - kadi                  # Switch to the kadi user
    kadi search reindex -m template # Reindex all existing template data

  Note that interrupting the command may lead to orphaned indices being created, which
  can be deleted manually by using:

  .. code-block:: bash

    sudo su - kadi             # Switch to the kadi user
    kadi search ls             # List all search indices
    kadi search remove <index> # Remove all search indices specified by <index>

Version 0.21.0
~~~~~~~~~~~~~~

* **Before the update:** The ``FOOTER_NAV_ITEMS`` :ref:`configuration
  <installation-configuration-settings>` item has been renamed to ``NAV_FOOTER_ITEMS``,
  which uses a more simple structure than before and can also be configured via the
  graphical sysadmin interface. For more complex customization needs, e.g. custom
  styling or translations, the new :func:`kadi.plugins.spec.kadi_get_nav_footer_items`
  plugin hook may be used instead.

Version 0.19.0
~~~~~~~~~~~~~~

* **Before the update:** The installation of uWSGI is now done via pip instead of APT in
  the installation instructions and script. This will usually install a newer version
  and will also make the use of Python versions different from the system installation
  easier. Therefore, existing installations should switch by performing the following
  steps:

  .. code-block:: bash

      sudo systemctl stop apache2 uwsgi kadi-celery kadi-celerybeat # Stop all services
      sudo systemctl disable uwsgi                                  # Disable the old uWSGI service
      sudo apt install libpcre3-dev                                 # Install an additional dependency needed to install uWSGI

  Simply disabling the old uWSGI service should be sufficient. Afterwards, the new
  version of uWSGI can be installed via pip by simply updating the application code
  :ref:`as usual <installation-production-updating-performing>`. Note that the services
  should not be restarted yet.

  To generate a new basic configuration for uWSGI, the Kadi CLI can be used:

  .. code-block:: bash

      sudo su - kadi                                # Switch to the kadi user
      kadi utils uwsgi --out ${HOME}/kadi-uwsgi.ini # Generate a configuration file for uWSGI

  The generated configuration should be rechecked as further customization may be
  necessary, especially in case any customizations were done to it in the past. Note
  that the old configuration file can be found at ``/etc/uwsgi/apps-available/kadi.ini``
  when using the installation instructions or script.

  Once the configuration is suitable, it should be moved to a suitable place:

  .. code-block:: bash

      sudo mv /opt/kadi/kadi-uwsgi.ini /etc/

  This installation of uWSGI also needs an additional systemd unit file:

  .. code-block:: bash

      sudo su - kadi                                            # Switch to the kadi user
      kadi utils uwsgi-service --out ${HOME}/kadi-uwsgi.service # Generate a systemd unit file for uWSGI

  Again, the generated configuration should be rechecked as further customization may be
  necessary. Once the configuration is suitable, it can be enabled and started like
  this:

  .. code-block:: bash

      sudo mv /opt/kadi/kadi-uwsgi.service /etc/systemd/system/
      sudo systemctl daemon-reload
      sudo systemctl enable kadi-uwsgi

  Afterwards, the services can be started again :ref:`as usual
  <installation-production-updating-performing>`, if no additional update steps need to
  be performed beforehand. In case something related to uWSGI does not work, all
  relevant errors should end up in the log files at ``/var/log/uwsgi``, as usual.
* **Before the update:** Support for Python version **3.6** has been removed. For
  existing installations that use this version, please see the instructions on how to
  install and use a :ref:`different Python version <installation-production-python>`.

Version 0.16.0
~~~~~~~~~~~~~~

* **Before the update:** The uWSGI configuration template has been adapted. When using a
  configuration such as described in the installation instructions or as generated by
  the installation script, the following lines should be added to
  ``/etc/uwsgi/apps-available/kadi.ini``:

  .. code-block::

    buffer-size = 32768
    post-buffering = 32768

  These values ensure that the application can properly deal with larger query
  parameters and slow uploads in certain cases.
* **After the update:** The existing search mappings for records, collections and groups
  have been changed in order to improve exact matches and searches with short queries.
  To apply the new mappings in existing installations, existing data has to be reindexed
  using the Kadi CLI by running:

  .. code-block:: bash

    sudo su - kadi      # Switch to the kadi user
    kadi search reindex # Reindex all existing data

  Note that the existing indices should still be searchable while the command is running
  (which may take a while), so the operation can also be performed while the application
  is already running. Once the command finishes, the old indices are deleted and
  switched with the new ones afterwards. Note that interrupting the command may lead to
  orphaned indices being created, which can be deleted manually by using:

  .. code-block:: bash

    sudo su - kadi             # Switch to the kadi user
    kadi search ls             # List all search indices
    kadi search remove <index> # Remove all search indices specified by <index>

Version 0.15.0
~~~~~~~~~~~~~~

* **Before the update:** The Python package ``psycopg2-binary``, to interact with the
  PostgreSQL database, has been replaced with ``psycopg2``, as the former is not
  actually supposed to be used in productive environments. As the new package needs to
  be built from source, it requires some additional build prerequisites. For existing
  installations, these can be installed via APT by running the following command:

  .. code-block:: bash

    sudo apt install build-essential python3-dev libpq-dev

Version 0.14.0
~~~~~~~~~~~~~~

* **Before the update:** The Elasticsearch configuration that is described in the
  installation instructions and also used in the installation script, which assume a
  simple, single-node setup, has been adjusted. For existing installations that use this
  kind of setup, the necessary changes to the configuration file of Elasticsearch can be
  applied by running the following command:

  .. code-block:: bash

    echo -e "discovery.type: single-node\nxpack.security.enabled: false" | sudo tee -a /etc/elasticsearch/elasticsearch.yml

  For the changes to take effect, Elasticsearch has to be restarted using:

  .. code-block:: bash

    systemctl restart elasticsearch
