.. _installation-production-script:

Installation via setup script
=============================

The setup bash script to install |kadi| can always be found on GitLab, e.g. the latest
stable version is available |install-script-link|. It is supposed to be run on a freshly
installed (virtual) machine.

.. note::
    The installation script will create a new system user called ``kadi``. Please make
    sure that there are no existing users with this name on the machine where the script
    is executed.

The following commands can be used to easily run the script from the command line:

.. code-block:: bash
    :substitutions:

    sudo apt install curl
    curl |install-script| > kadi_setup.sh
    less kadi_setup.sh # Let's get an overview about what it does before running it as root
    chmod +x kadi_setup.sh
    sudo ./kadi_setup.sh

.. note::
    If the script exits prematurely due to an unsuitable Python version, please see the
    instructions on how to install a :ref:`different Python version
    <installation-production-python>`. Afterwards, the script can make use of the newly
    installed Python version by specifying the corresponding Python interpreter to use:

    .. code-block:: bash

        sudo ./kadi_setup.sh python3.x

The script should mostly run through autonomously, prompting for user input at some
steps where necessary. Once it is complete, some further post-installation steps will
most likely be required, which the script will notify about.
