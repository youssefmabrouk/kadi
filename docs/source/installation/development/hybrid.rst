.. _installation-development-hybrid:

Hybrid installation
===================

This installation method is almost identical to the :ref:`manual
<installation-development-manual>` installation, so please refer to that chapter first.
The only difference is that some of the dependent services, namely PostgreSQL, Redis and
optionally Elasticsearch, are run via Docker containers and do not need to be installed
separately.

Installing the dependencies
---------------------------

The only additional dependency that is needed for this type of installation is `Docker
<https://www.docker.com>`__. For this step, please refer to the installation via
:ref:`Docker <installation-development-docker>`.

Running the services
--------------------

PostgreSQL can be run via the following command, which will expose port 5432 of the
container and persist all data in a named volume called ``postgres``:

.. code-block:: bash

    sudo docker run --name postgres -d -p 5432:5432 -v postgres:/var/lib/postgresql/data -e POSTGRES_DB=kadi -e POSTGRES_USER=kadi -e POSTGRES_PASSWORD=kadi postgres:15

Redis can be run via the following command, which will expose port 6379 of the container
and persist all data in a named volume called ``redis``:

.. code-block:: bash

    sudo docker run --name redis -d -p 6379:6379 -v redis:/data redis:7

Elasticsearch can optionally be run via the following command, which will expose port
9200 of the container and persist all data in a named volume called ``elasticsearch``:

.. code-block:: bash

    sudo docker run --name elasticsearch -d -p 9200:9200 -v elasticsearch:/usr/share/elasticsearch/data -e discovery.type=single-node -e xpack.security.enabled=false -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" elasticsearch:8.10.2

For more information about managing the containers, please refer to the official
documentation of `Docker <https://docs.docker.com>`__.
