.. _installation-plugins:

Plugins
=======

This section explains how plugins can be installed and configured to extend the
functionality of |kadi|. It is recommended to have a working |kadi|
:ref:`installation <installation>` before reading through this section. For general
information about configuring |kadi|, please also see :ref:`Configuration
<installation-configuration>`.

Installing plugins
------------------

Plugins can either be part of the main application package or implemented as independent
Python packages, which have to be installed separately in the same virtual environment
that |kadi| itself was installed in. In both cases, plugins are configured via the
``PLUGINS`` configuration value in the Kadi configuration file after they have been
installed:

.. code-block:: python3

    PLUGINS = ["example", "another-example"]

This will activate the plugins with the names ``example`` and ``another-example`` in the
application.

Some plugins might require additional configuration to work correctly or to change how
they behave. This configuration is plugin-specific and can be specified via the
``PLUGIN_CONFIG`` configuration value in the same configuration file. Note that the
configuration always needs to be a dictionary, e.g. like the following configuration for
the ``example`` plugin:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "example": {
            "example_key": "example_value",
        },
    }

First party plugins
-------------------

Currently, all first party plugins are part of the main application package and are
therefore available automatically after installing |kadi|.

Zenodo
~~~~~~

The ``zenodo`` plugin allows users to connect their |kadi| account to a `Zenodo
<https://zenodo.org>`__ account in order to upload records directly to the Zenodo
repository. The following configuration values have to be specified when using this
plugin:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "zenodo": {
            "client_id": "<zenodo_client_id>",         # Required
            "client_secret": "<zenodo_client_secret>", # Required
            "base_url": "https://sandbox.zenodo.org",  # Optional
        }
    }

The ``client_id`` and ``client_secret`` can be obtained directly via Zenodo by
navigating to *Applications > Developer Applications* in the account settings page.
Besides setting some metadata that will be displayed to users of the particular instance
of |kadi|, the following settings are necessary for the integration to work correctly:

 * Redirect URIs: ``https://<base_url>/settings/services/authorize/zenodo``
 * Client type: ``Confidential``

The ``<base_url>`` in the redirect URI value is the base URL of the |kadi| instance to
use, e.g. ``kadi4mat.example.edu``, not the URL of the configured Zenodo instance.

The ``base_url`` in the plugin config on the other hand specifies the URL of the Zenodo
instance to use, which defaults to ``"https://zenodo.org"``. For testing purposes, the
`Zenodo sandbox <https://sandbox.zenodo.org>`__ can be used instead, as configured in
the example above.

TIB TS
~~~~~~

The ``tib_ts`` plugin provides a search for term IRIs using the `TIB Terminology Service
<https://terminology.tib.eu/ts>`__ in all places where term IRIs can be specified. The
plugin works without any prior configuration, but supports configuring a custom API
endpoint:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "tib_ts": {
            "endpoint": "https://service.tib.eu/ts4tib/api/search", # Optional
        }
    }

The ``endpoint`` corresponds to the term search API endpoint provided by the service and
defaults to the value shown in the example above.

InfluxDB
~~~~~~~~

The ``influxdb`` plugin allows sysadmins to configure one or more `InfluxDB
<https://www.influxdata.com/>`__ instances, which allows users to query them through the
API of |kadi|. The following configuration values have to be specified when using this
plugin:

.. code-block:: python3

    PLUGIN_CONFIG = {
        "influxdb": {
            "<name>": {              # Required
                "url": "<base_url>", # Required
                "title": "<title>",  # Optional
                "timeout": 10,       # Optional
                "token": "<token>",  # Optional
                "groups": [],        # Optional
            }
        }
    }

Each instance requires a unique ``name``, containing the instance-specific configuration
as dictionary. The only required value within this dictionary is the ``url``, specifying
the base URL (i.e. without ``/api/...``) of the InfluxDB instance to use.

The ``title`` specifies how an InfluxDB instance to be configured by a user is displayed
in the UI, which defaults to the configured ``name``.

The ``timeout`` value specifies the maximum amount of seconds a proxied request to an
InfluxDB instance can last before it is aborted. If not specified, a default timeout of
``10`` seconds is used.

``token`` configures a global token for all users. If a user token already exists for a
given instance, it is ignored and the global token is used instead.

.. warning::
    Be careful which scopes and buckets are accessible with the provided global token.

``groups`` allows specifying a single group ID or a list of group IDs within |kadi|,
which restricts access to the InfluxDB instance to these groups. It is recommended to
only specify groups that are manageable by the sysadmin.

Once set up, users will be able to configure their connection to an InfluxDB instance by
providing a corresponding API token in their user preferences, which is stored encrypted
in |kadi|. Afterwards, the InfluxDB instance can be queried using the proxy endpoint
``/api/influxdb/<name>/query?orgID=<orgID>`` of |kadi|, with the ``<name>`` replaced
by the corresponding identifier of the configured InfluxDB instance and the ``<orgID>``
replaced by an organization ID within InfluxDB.
