Using different Python versions
===============================

The installation instructions assume that a recent enough version of Python can be
installed using APT. As sometimes this is not the case, and updating the whole operating
system is not always an option, Python can usually be built from source to get a more
recent version in addition to the system installation of Python.

Compiling Python from source
----------------------------

First, some additional build dependencies need to be installed:

.. code-block:: bash

    sudo apt install make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

The source of the desired Python version can then be downloaded from the `Python
download page <https://www.python.org/downloads/source>`__, e.g. by using wget:

.. code-block:: bash

    wget https://www.python.org/ftp/python/3.x.y/Python-3.x.y.tgz

.. note::
    When compiling from source, it is usually recommended to use the latest Python
    version supported by |kadi|, which currently corresponds to version **3.11**.

Afterwards, the source package can be extracted and built using make. Using the ``-j``
flag of make speeds up compilation by making use of ``<n>`` multiple jobs in parallel,
which usually should correspond to the number of cores available:

.. code-block:: bash

    tar -xf Python-3.x.y.tgz
    cd Python-3.x.y
    ./configure --enable-optimizations --with-lto
    make -j <n>

Note that this may take a bit. Once built, in order to install the compiled binary as an
alternative to the one already installed on the system, the ``altinstall`` target can be
used:

.. warning::
    Make sure to actually use the ``altinstall`` target instead of ``install``, as the
    latter may have unintended side effects.

.. code-block:: bash

    sudo make altinstall

This will install the Python binary as ``python3.x``, depending on the chosen version,
which can be verified by running:

.. code-block:: bash

    python3.x --version
