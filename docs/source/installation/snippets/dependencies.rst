Installing the dependencies
---------------------------

Python and Virtualenv
~~~~~~~~~~~~~~~~~~~~~

As the backend code of the application is based on the `Flask
<https://flask.palletsprojects.com>`__ web framework and multiple other Python
libraries, Python 3 needs to be installed. This should generally be the case already,
otherwise it can be installed using:

.. code-block:: bash

    sudo apt install python3

.. note::
    Note that a Python version **>=3.8** and **<3.12** is required. The currently
    installed version can be checked using:

    .. code-block:: bash

        python3 --version

    If the currently installed Python version is not suitable, please see the
    instructions on how to install and use a different Python version for
    :ref:`production <installation-production-python>` or :ref:`development
    <installation-development-python>` installations.

To create an isolated environment for the application and its dependencies, `Virtualenv
<https://virtualenv.pypa.io/en/stable>`__ is used, which can be installed like this:

.. code-block:: bash

    sudo apt install virtualenv

Libraries
~~~~~~~~~

Some external libraries and tools are required as additional dependencies, which can be
installed using:

.. code-block:: bash

    sudo apt install libmagic1 build-essential python3-dev python3-venv libpq-dev libpcre3-dev

PostgreSQL
~~~~~~~~~~

The RDBMS used in the application is `PostgreSQL <https://www.postgresql.org>`__. Any up
to date version **>=13** should work, which can be installed like this:

.. code-block:: bash

    sudo apt install postgresql

Redis
~~~~~

`Redis <https://redis.io>`__ is an in-memory data structure that can be used for
different purposes. Currently it is used as cache for request rate limiting and as a
message broker for running asynchronous tasks with `Celery
<https://docs.celeryq.dev/en/stable/>`__, a distributed task queue. Any up to date
version **>=6** should work, which can be installed like this:

.. code-block:: bash

    sudo apt install redis-server
