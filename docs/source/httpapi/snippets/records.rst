To get more detailed information about some the contents of the requested/returned
representation of resources related to records, see also :class:`.Record`, especially
:attr:`.Record.extras`, :class:`.RecordLink`, :class:`.File`, :class:`.Upload` and
:class:`.Revision`. Note that for tags (:class:`.Tag`) and licenses (:class:`.License`),
only their names are relevant for the API. For possible license and role values, see
also the :ref:`Miscellaneous <httpapi-latest-misc>` endpoints.
