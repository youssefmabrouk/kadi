To get more detailed information about some the contents of the requested/returned
representation of resources related to groups, see also :class:`.Group` and
:class:`.Revision`. For possible role values, see also the :ref:`Miscellaneous
<httpapi-latest-misc>` endpoints.
