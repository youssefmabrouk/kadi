To get more detailed information about some the contents of the requested/returned
representation of resources, see also :class:`.License`, :class:`.Role`,
:class:`.Permission` and :class:`.Tag`.
