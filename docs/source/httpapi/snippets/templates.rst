To get more detailed information about some the contents of the requested/returned
representation of resources related to templates, see also :class:`.Template`, especially
:attr:`.Template.data`, and :class:`.Revision`. For possible role values, see also the
:ref:`Miscellaneous <httpapi-latest-misc>` endpoints.
