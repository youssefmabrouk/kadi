.. _apiref-lib:

Library
=======

This section contains a complete API reference of the :mod:`kadi.lib` Python module.

API
---

.. automodule:: kadi.lib.api.blueprint
    :members:

.. automodule:: kadi.lib.api.core
    :members:

.. automodule:: kadi.lib.api.models
    :members:
    :exclude-members: AccessTokenMixin, query

.. automodule:: kadi.lib.api.schemas
    :members:

.. automodule:: kadi.lib.api.utils
    :members:

Cache
-----

.. automodule:: kadi.lib.cache
    :members:

Config
------

.. automodule:: kadi.lib.config.core
    :members:

.. automodule:: kadi.lib.config.models
    :members:
    :exclude-members: query

Conversion
----------

.. automodule:: kadi.lib.conversion
    :members:

Database
--------

.. automodule:: kadi.lib.db
    :members:

Exceptions
----------

.. automodule:: kadi.lib.exceptions
    :members:

Export
------

.. automodule:: kadi.lib.export
    :members:

Favorites
---------

.. automodule:: kadi.lib.favorites.core
    :members:

.. automodule:: kadi.lib.favorites.models
    :members:
    :exclude-members: query

Format
------

.. automodule:: kadi.lib.format
    :members:

Forms
-----

.. automodule:: kadi.lib.forms
    :members:

Jinja
-----

.. automodule:: kadi.lib.jinja
    :members:

Licenses
--------

.. automodule:: kadi.lib.licenses.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.licenses.schemas
    :members:

.. automodule:: kadi.lib.licenses.utils
    :members:

LDAP
----

.. automodule:: kadi.lib.ldap
    :members:

Mails
-----

.. automodule:: kadi.lib.mails.core
    :members:

.. automodule:: kadi.lib.mails.tasks
    :members:

.. automodule:: kadi.lib.mails.utils
    :members:

Notifications
-------------

.. automodule:: kadi.lib.notifications.core
    :members:

.. automodule:: kadi.lib.notifications.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.notifications.schemas
    :members:

OAuth
-----

.. automodule:: kadi.lib.oauth.core
    :members:

.. automodule:: kadi.lib.oauth.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.oauth.schemas
    :members:

.. automodule:: kadi.lib.oauth.utils
    :members:

Permissions
-----------

.. automodule:: kadi.lib.permissions.core
    :members:

.. automodule:: kadi.lib.permissions.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.permissions.schemas
    :members:

.. automodule:: kadi.lib.permissions.tasks
    :members:

.. automodule:: kadi.lib.permissions.utils
    :members:

Plugins
-------

.. automodule:: kadi.lib.plugins.core
    :members:

.. automodule:: kadi.lib.plugins.utils
    :members:

Publication
-----------

.. automodule:: kadi.lib.publication
    :members:

Resources
---------

.. automodule:: kadi.lib.resources.api
    :members:

.. automodule:: kadi.lib.resources.core
    :members:

.. automodule:: kadi.lib.resources.forms
    :members:

.. automodule:: kadi.lib.resources.schemas
    :members:

.. automodule:: kadi.lib.resources.tasks
    :members:

.. automodule:: kadi.lib.resources.utils
    :members:

.. automodule:: kadi.lib.resources.views
    :members:

Revisions
---------

.. automodule:: kadi.lib.revisions.core
    :members:

.. automodule:: kadi.lib.revisions.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.revisions.schemas
    :members:

.. automodule:: kadi.lib.revisions.utils
    :members:

Schemas
-------

.. automodule:: kadi.lib.schemas
    :members:

Search
------

.. automodule:: kadi.lib.search.core
    :members:

.. automodule:: kadi.lib.search.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.search.schemas
    :members:

Security
--------

.. automodule:: kadi.lib.security
    :members:

Storage
-------

.. automodule:: kadi.lib.storage.core
    :members:

.. automodule:: kadi.lib.storage.local
    :members:

.. automodule:: kadi.lib.storage.misc
    :members:

.. automodule:: kadi.lib.storage.schemas
    :members:

Tags
----

.. automodule:: kadi.lib.tags.core
    :members:

.. automodule:: kadi.lib.tags.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.tags.schemas
    :members:

Tasks
-----

.. automodule:: kadi.lib.tasks.core
    :members:

.. automodule:: kadi.lib.tasks.models
    :members:
    :exclude-members: query

.. automodule:: kadi.lib.tasks.utils
    :members:

Utils
-----

.. automodule:: kadi.lib.utils
    :members:

Validation
----------

.. automodule:: kadi.lib.validation
    :members:

Web
---

.. automodule:: kadi.lib.web
    :members:
