/* Copyright 2020 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

const glob = require('glob');
const path = require('path');

const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const scriptsDir = 'kadi/assets/scripts/';
const entryPoints = {main: `${scriptsDir}main.js`};

glob.sync(`${scriptsDir}app/**/*.js`).forEach((fileName) => {
  entryPoints[fileName.substring(scriptsDir.length, fileName.length - 3)] = fileName;
});

function sourceMapFilename(info, fallback = false) {
  let absPath = info.absoluteResourcePath;
  // Manually try to bend some incorrect paths into shape.
  absPath = absPath.replace('ignored|', '').replace('|.', '');
  return `webpack:///${path.relative(__dirname, absPath)}${fallback ? info.hash : ''}`;
}

module.exports = {
  devtool: 'source-map',
  entry: entryPoints,
  output: {
    path: path.join(__dirname, 'kadi/static/dist'),
    devtoolModuleFilenameTemplate: (info) => sourceMapFilename(info),
    devtoolFallbackModuleFilenameTemplate: (info) => sourceMapFilename(info, true),
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          {loader: 'css-loader', options: {esModule: false}},
          'postcss-loader',
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          {loader: 'css-loader', options: {esModule: false}},
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\/styles\/main\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {loader: 'css-loader', options: {esModule: false}},
          'postcss-loader',
          'resolve-url-loader',
          'sass-loader',
        ],
      },
      {
        test: /fa-.*\.(ttf|woff2)$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name]-v6.4.2[ext]',
        },
      },
      {
        test: /lato-.*\.woff2?$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name]-v3.0.0[ext]',
        },
      },
      {
        test: /KaTeX_.*\.(ttf|woff2?)$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name]-v0.16.8[ext]',
        },
      },
    ],
  },
  performance: {
    hints: false,
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanAfterEveryBuildPatterns: ['!fonts/**/*'],
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
    new VueLoaderPlugin(),
  ],
  resolve: {
    alias: {
      vue: 'vue/dist/vue.esm.js',
    },
    modules: ['.', './node_modules'],
  },
};
