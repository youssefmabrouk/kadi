#!/usr/bin/env bash
set -e

#############
# Variables #
#############

PYTHON_INTERPRETER=${1:-python3}
PYTHON_VERSION_LOWER="8"
PYTHON_VERSION_UPPER="12"

KADI_USER="kadi"
KADI_GROUP="www-data"
KADI_HOME="/opt/kadi"
KADI_CONFIG_FILE="${KADI_HOME}/config/kadi.py"

STORAGE_PATH_DEFAULT="${KADI_HOME}/storage"
MISC_UPLOADS_PATH_DEFAULT="${KADI_HOME}/uploads"

UWSGI_CONFIG_FILE="/etc/kadi-uwsgi.ini"
APACHE_CONFIG_FILE="/etc/apache2/sites-available/kadi.conf"

CERT_FILE="/etc/ssl/certs/kadi.crt"
KEY_FILE="/etc/ssl/private/kadi.key"

DB_NAME="kadi"
DB_USER="kadi"
DB_PASSWORD="$(tr -dc '[:alnum:]' < /dev/urandom | head -c20)"

#############
# Functions #
#############

info() {
  echo ""
  tput bold
  echo "$1"
  tput sgr0
}

kadi() {
  sudo -Hiu ${KADI_USER} bash -c "$1"
}

postgres() {
  sudo -Hiu postgres psql -c "$1" > /dev/null
}

config() {
  echo "$1" >> ${KADI_CONFIG_FILE}
}

########
# Main #
########

setup() {

if [[ ${EUID} > 0 ]]; then
  echo "This script must be run as root."
  exit 1
fi


info "Adding the Elasticsearch repository..."

ES_SOURCE_LIST="/etc/apt/sources.list.d/elastic-8.x.list"

if [[ ! -f ${ES_SOURCE_LIST} ]]; then
  apt update
  apt install -y wget apt-transport-https gnupg
  wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
  echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" >> ${ES_SOURCE_LIST}
  echo "Repository added."
else
  echo "Repository already exists."
fi


info "Installing dependencies..."

apt update
apt install -y libmagic1 build-essential python3-dev python3-venv libpq-dev libpcre3-dev openssl python3 postgresql \
               redis-server elasticsearch apache2 libapache2-mod-proxy-uwsgi libapache2-mod-xsendfile


info "Verifying Python version..."

if [[ ! $(which ${PYTHON_INTERPRETER}) ]]; then
  tput setaf 1
  echo "Python interpreter '${PYTHON_INTERPRETER}' could not be found."
  tput sgr0
  exit 1
fi

echo "Using Python interpreter '${PYTHON_INTERPRETER}'."

if [[ $(${PYTHON_INTERPRETER} -c "import sys;print(${PYTHON_VERSION_LOWER}<=sys.version_info.minor<${PYTHON_VERSION_UPPER})") == "True" ]]; then
  echo "Currently used Python version is >=3.${PYTHON_VERSION_LOWER} and <3.${PYTHON_VERSION_UPPER}."
else
  tput setaf 3
  echo "The currently used Python version is not suitable, Python version >=3.${PYTHON_VERSION_LOWER} and <3.${PYTHON_VERSION_UPPER} is required."
  tput sgr0
  echo "Please see the installation instructions on how to install and use a newer version."
  exit 1
fi


info "Configuring and starting Elasticsearch..."

ES_CONFIG_FILE="/etc/elasticsearch/elasticsearch.yml"

if [[ ! $(grep "discovery.type: single-node" ${ES_CONFIG_FILE}) ]]; then
  echo -e "discovery.type: single-node\nxpack.security.enabled: false" | sudo tee -a ${ES_CONFIG_FILE}
else
  echo "Elasticsearch is already configured."
fi

systemctl enable elasticsearch.service
systemctl start elasticsearch.service


info "Setting up '${KADI_USER}' user..."

if ! id ${KADI_USER} &> /dev/null; then
  adduser ${KADI_USER} --system --home ${KADI_HOME} --ingroup ${KADI_GROUP} --shell /bin/bash
  sudo -Hu ${KADI_USER} bash -c 'echo "export KADI_CONFIG_FILE=\${HOME}/config/kadi.py" >> ~/.profile'
  sudo -Hu ${KADI_USER} bash -c 'echo "test -z \"\${VIRTUAL_ENV}\" && source \${HOME}/venv/bin/activate" >> ~/.profile'
else
  echo "User already exists."
fi


info "Setting up virtual environment..."

if [[ ! -d ${KADI_HOME}/venv ]]; then
  sudo -Hu ${KADI_USER} bash -c "${PYTHON_INTERPRETER} -m venv \${HOME}/venv"
  echo "Virtual environment created."
else
  echo "Virtual environment already exists."
fi


info "Installing Kadi..."

kadi 'pip install wheel'
kadi 'pip install -U pip'
kadi 'pip install kadi'


info "Setting up database user '${DB_USER}'..."

if [[ ! $(sudo -Hiu postgres psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='${DB_USER}'") ]]; then
  postgres "CREATE USER ${DB_USER} WITH PASSWORD '${DB_PASSWORD}';"
  echo "Database user created."
else
  DB_EXISTS=true
  echo "Database user already exists."
fi


info "Setting up database '${DB_NAME}'..."

if [[ ! $(sudo -Hiu postgres psql postgres -tAc "SELECT 1 FROM pg_database WHERE datname='${DB_NAME}'") ]]; then
  postgres "CREATE DATABASE ${DB_NAME} WITH OWNER ${DB_USER} ENCODING 'UTF-8' TEMPLATE template0;"
  echo "Database created."
else
  echo "Database already exists."
fi


info "Creating Kadi configuration file..."

if [[ ! -f ${KADI_CONFIG_FILE} ]]; then
  while [[ -z ${SERVER_NAME} ]]; do
    read -p "Enter the name or IP of the host: " SERVER_NAME
  done

  read -p "Enter the path for storing data [${STORAGE_PATH_DEFAULT}]: " STORAGE_PATH
  [[ -z ${STORAGE_PATH} ]] && STORAGE_PATH=${STORAGE_PATH_DEFAULT}

  mkdir -p ${STORAGE_PATH}
  chown ${KADI_USER}:${KADI_GROUP} ${STORAGE_PATH}
  chmod 750 ${STORAGE_PATH}

  read -p "Enter the path for storing miscellaneous uploads (e.g. profile or group pictures) [${MISC_UPLOADS_PATH_DEFAULT}]: " MISC_UPLOADS_PATH
  [[ -z ${MISC_UPLOADS_PATH} ]] && MISC_UPLOADS_PATH=${MISC_UPLOADS_PATH_DEFAULT}

  mkdir -p ${MISC_UPLOADS_PATH}
  chown ${KADI_USER}:${KADI_GROUP} ${MISC_UPLOADS_PATH}
  chmod 750 ${MISC_UPLOADS_PATH}

  if [[ -n ${DB_EXISTS} ]]; then
    postgres "ALTER USER ${DB_USER} WITH PASSWORD '${DB_PASSWORD}';"
  fi

  kadi 'mkdir -p ${HOME}/config'
  kadi "touch ${KADI_CONFIG_FILE}"
  kadi "chmod 640 ${KADI_CONFIG_FILE}"

  config "SQLALCHEMY_DATABASE_URI = \"postgresql://${DB_USER}:${DB_PASSWORD}@localhost/${DB_NAME}\""
  config "STORAGE_PATH = \"${STORAGE_PATH}\""
  config "MISC_UPLOADS_PATH = \"${MISC_UPLOADS_PATH}\""
  config "SERVER_NAME = \"${SERVER_NAME}\""
  config "SECRET_KEY = \"$(tr -dc '[:alnum:]' < /dev/urandom | head -c20)\""
  config "SMTP_HOST = \"localhost\""
  config "SMTP_PORT = 25"
  config "SMTP_USERNAME = \"\""
  config "SMTP_PASSWORD = \"\""
  config "SMTP_USE_TLS = False"
  config "MAIL_NO_REPLY = \"no-reply@${SERVER_NAME}\""

  echo "Kadi configuration file created."
else
  SERVER_NAME=$(awk -F "=" '/SERVER_NAME/ {print $2}' ${KADI_CONFIG_FILE} | tr -d '" ')
  echo "Kadi configuration file already exists."
fi


info "Configuring uWSGI..."

if [[ ! -f ${UWSGI_CONFIG_FILE} ]]; then
  kadi "kadi utils uwsgi --default --out ${KADI_HOME}/kadi-uwsgi.ini"
  mv ${KADI_HOME}/kadi-uwsgi.ini /etc/

  kadi "kadi utils uwsgi-service --default --out ${KADI_HOME}/kadi-uwsgi.service"
  mv ${KADI_HOME}/kadi-uwsgi.service /etc/systemd/system/

  systemctl daemon-reload
  systemctl enable kadi-uwsgi

  echo -e "/var/log/uwsgi/*.log {\n  copytruncate\n  compress\n  delaycompress\n  missingok\n  notifempty\n  rotate 10\n  weekly\n}" \
    > /etc/logrotate.d/uwsgi
else
  echo "uWSGI is already configured."
fi


info "Configuring Apache..."

if [[ ! -f ${APACHE_CONFIG_FILE} ]]; then
  openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ${KEY_FILE} -out ${CERT_FILE} \
              -subj "/CN=${SERVER_NAME}" -addext "subjectAltName=DNS:${SERVER_NAME}"

  kadi "kadi utils apache --default --out ${KADI_HOME}/kadi.conf"
  mv ${KADI_HOME}/kadi.conf /etc/apache2/sites-available/

  a2dissite 000-default
  a2ensite kadi
  a2enmod proxy_uwsgi ssl xsendfile headers
else
  echo "Apache is already configured."
fi


info "Configuring Celery and Celery Beat..."

if [[ ! -f "/etc/systemd/system/kadi-celery.service" ]]; then
  kadi "kadi utils celery --default --out ${KADI_HOME}/kadi-celery.service"
  kadi "kadi utils celerybeat --default --out ${KADI_HOME}/kadi-celerybeat.service"

  mv ${KADI_HOME}/kadi-celery.service /etc/systemd/system/
  mv ${KADI_HOME}/kadi-celerybeat.service /etc/systemd/system/

  systemctl daemon-reload
  systemctl enable kadi-celery kadi-celerybeat

  echo -e "/var/log/celery/*.log {\n  copytruncate\n  compress\n  delaycompress\n  missingok\n  notifempty\n  rotate 10\n  weekly\n}" \
    > /etc/logrotate.d/celery
else
  echo "Celery is already configured."
fi


info "Setting up Kadi..."

kadi 'kadi db init'
kadi 'kadi search init'


info "Restarting services..."

systemctl restart apache2 kadi-uwsgi kadi-celery kadi-celerybeat
echo "Restarted services."


echo ""
tput bold
tput setaf 2
echo "Setup finished successfully!"
tput sgr0
tput bold
echo "After ensuring Kadi is reachable at 'https://${SERVER_NAME}', please also check and adjust the Kadi configuration file at '${KADI_CONFIG_FILE}'."
tput sgr0
echo "If Kadi is not reachable, please ensure that the default HTTP(s) ports 80 and 443 are accessible."
echo ""
tput bold
echo "Please also note the following:"
tput sgr0
echo "The web server is currently using a self-signed certificate, which should only be used internally." \
     "Once you want to use another certificate, make sure to adjust the corresponding values in the Apache configuration file at '${APACHE_CONFIG_FILE}'." \
     "The generated certificate file ('${CERT_FILE}') and key file ('${KEY_FILE}') can be safely deleted afterwards." \
     "Please also refer to the manual production installation instructions in the documentation for more details."

}

setup
