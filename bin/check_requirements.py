#!/usr/bin/env python3
import os
import subprocess
import sys

import click
from pyproject_parser import PyProject


def _get_max_len(header, requirements, key=None):
    if len(requirements) == 0:
        return len(header)

    if key is None:
        max_value_len = max(len(package) for package in requirements)
    else:
        max_value_len = max(len(item[key]) for item in requirements.values())

    return max(len(header), max_value_len)


@click.command()
def check_requirements():
    """Check all Python dependencies for updates."""
    requirements = {}

    filepath = os.path.join(os.path.dirname(__file__), "..", "pyproject.toml")
    project = PyProject.load(filepath).project

    for dependency in project["dependencies"]:
        version = str(dependency.specifier).replace("==", "")
        requirements[dependency.name] = {
            "extra": "-",
            "current": version,
            "latest": None,
        }

    for extra, dependencies in project["optional-dependencies"].items():
        for dependency in dependencies:
            version = str(dependency.specifier).replace("==", "")
            requirements[dependency.name] = {
                "extra": extra,
                "current": version,
                "latest": None,
            }

    # Get the latest versions of all outdated packages via pip. Suppress stderr in order
    # to ignore non-relevant output.
    result = subprocess.run(
        ["pip", "list", "--outdated"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL
    )
    lines = [line.strip() for line in result.stdout.decode().splitlines()]

    # Ignore the first two header lines when iterating.
    for line in lines[2:]:
        # Stop after the first empty line, in case there is more output after the
        # package list.
        if not line:
            break

        parts = line.split()
        package = parts[0].lower()

        if package in requirements:
            requirements[package]["latest"] = parts[2]

    # Remove all packages that are already up to date. Make a copy of the keys so we can
    # mutate the requirements while iterating.
    for package in list(requirements):
        requirement_meta = requirements[package]

        if (
            not requirement_meta["latest"]
            or requirement_meta["current"] == requirement_meta["latest"]
        ):
            del requirements[package]

    if len(requirements) == 0:
        click.echo("All requirements are up to date.")
        sys.exit(0)

    # Print all packages and versions.
    package_header = "Package"
    extra_header = "Extra"
    current_ver_header = "Current"
    latest_ver_header = "Latest"

    package_len = _get_max_len(package_header, requirements)
    extra_len = _get_max_len(extra_header, requirements, "extra")
    current_ver_len = _get_max_len(current_ver_header, requirements, "current")
    latest_ver_len = _get_max_len(latest_ver_header, requirements, "latest")

    click.echo(
        f"{package_header.ljust(package_len)}"
        f"  {extra_header.ljust(extra_len)}"
        f"  {current_ver_header.ljust(current_ver_len)}"
        f"  {latest_ver_header.ljust(latest_ver_len)}"
    )
    click.echo(
        f"{'=' * package_len}"
        f"  {'=' * extra_len}"
        f"  {'=' * current_ver_len}"
        f"  {'=' * latest_ver_len}"
    )

    for package, requirement_meta in requirements.items():
        click.echo(
            f"{package.ljust(package_len)}"
            f"  {requirement_meta['extra'].ljust(extra_len)}"
            f"  {requirement_meta['current'].ljust(current_ver_len)}"
            f"  {requirement_meta['latest'].ljust(latest_ver_len)}"
        )


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    check_requirements()
