# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from .utils import initiate_upload
from .utils import upload_chunk
from .utils import upload_file
from kadi.lib.web import url_for
from kadi.modules.records.core import delete_record
from kadi.modules.records.models import Record
from kadi.modules.records.models import RecordLink
from kadi.modules.records.models import RecordState
from tests.modules.utils import check_api_post_subject_resource_role
from tests.utils import check_api_response


def test_new_record(api_client, dummy_personal_token):
    """Test the "api.new_record" endpoint."""
    response = api_client(dummy_personal_token).post(
        url_for("api.new_record"), json={"identifier": "test", "title": "test"}
    )

    check_api_response(response, status_code=201)
    assert Record.query.filter_by(identifier="test").one()


def test_new_record_link(api_client, dummy_personal_token, dummy_record, new_record):
    """Test the "api.new_record_link" endpoint."""
    record = new_record()

    response = api_client(dummy_personal_token).post(
        url_for("api.new_record_link", id=dummy_record.id),
        json={"name": "test", "record_to": {"id": record.id}},
    )

    check_api_response(response, status_code=201)
    assert dummy_record.links_to.filter(RecordLink.name == "test").one()


def test_add_record_collection(
    api_client, dummy_collection, dummy_personal_token, dummy_record
):
    """Test the "api.add_record_collection" endpoint."""
    response = api_client(dummy_personal_token).post(
        url_for("api.add_record_collection", id=dummy_record.id),
        json={"id": dummy_collection.id},
    )

    check_api_response(response, status_code=201)
    assert dummy_record.collections.one() == dummy_collection


def test_add_record_user_role(
    api_client, db, dummy_personal_token, dummy_record, new_user
):
    """Test the "api.add_record_user_role" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for("api.add_record_user_role", id=dummy_record.id)

    check_api_post_subject_resource_role(db, client, endpoint, new_user(), dummy_record)


def test_add_record_group_role(
    api_client, db, dummy_personal_token, dummy_record, dummy_group
):
    """Test the "api.add_record_group_role" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for("api.add_record_group_role", id=dummy_record.id)

    check_api_post_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_record
    )


def test_restore_record(api_client, dummy_personal_token, dummy_record, dummy_user):
    """Test the "api.restore_record" endpoint."""
    delete_record(dummy_record, user=dummy_user)

    response = api_client(dummy_personal_token).post(
        url_for("api.restore_record", id=dummy_record.id)
    )

    check_api_response(response)
    assert dummy_record.state == RecordState.ACTIVE


def test_purge_record(api_client, dummy_personal_token, dummy_record, dummy_user):
    """Test the "api.purge_record" endpoint."""
    delete_record(dummy_record, user=dummy_user)

    response = api_client(dummy_personal_token).post(
        url_for("api.purge_record", id=dummy_record.id)
    )

    check_api_response(response, status_code=202)
    assert Record.query.get(dummy_record.id) is None


def test_new_upload(api_client, dummy_personal_token, dummy_record):
    """Test the "api.new_upload" endpoint."""
    response = initiate_upload(
        api_client(dummy_personal_token),
        url_for("api.new_upload", id=dummy_record.id),
        file_data=10 * b"x",
    )
    check_api_response(response, status_code=201)


@pytest.mark.parametrize(
    "file_data,status_code",
    [
        (10 * b"x", 202),
        # Number of sent chunks won't match expected chunk count.
        (11 * b"x", 400),
    ],
)
def test_finish_upload(
    file_data,
    status_code,
    monkeypatch,
    api_client,
    dummy_personal_token,
    dummy_record,
):
    """Test the "api.finish_upload" endpoint."""
    chunk_size = 10
    monkeypatch.setitem(current_app.config, "UPLOAD_CHUNK_SIZE", chunk_size)

    client = api_client(dummy_personal_token)

    response = initiate_upload(
        client, url_for("api.new_upload", id=dummy_record.id), file_data=file_data
    )
    data = response.get_json()

    upload_chunk(
        client, data["_actions"]["upload_chunk"], chunk_data=file_data[:chunk_size]
    )

    response = client.post(data["_actions"]["finish_upload"])
    check_api_response(response, status_code=status_code)


@pytest.mark.parametrize(
    "size,checksum,status_code",
    [
        (None, None, 201),
        # Size mismatch.
        (10, None, 400),
        # Checksum mismatch.
        (None, "test", 400),
        # Size exceeds the direct upload limit.
        (11, None, 413),
    ],
)
def test_upload_file(
    size,
    checksum,
    status_code,
    monkeypatch,
    api_client,
    dummy_personal_token,
    dummy_record,
):
    """Test the "api.upload_file" endpoint."""
    monkeypatch.setitem(current_app.config, "UPLOAD_CHUNKED_BOUNDARY", 10)

    client = api_client(dummy_personal_token)

    response = upload_file(
        client,
        url_for("api.upload_file", id=dummy_record.id),
        size=size,
        checksum=checksum,
    )
    check_api_response(response, status_code=status_code)


@pytest.mark.parametrize("replace_file,status_code", [(False, 409), (True, 201)])
def test_upload_file_replace_file(
    replace_file,
    status_code,
    api_client,
    dummy_file,
    dummy_personal_token,
    dummy_record,
):
    """Test the "api.upload_file" endpoint when replacing files."""
    client = api_client(dummy_personal_token)

    response = upload_file(
        client,
        url_for("api.upload_file", id=dummy_record.id),
        name=dummy_file.name,
        replace_file=replace_file,
    )
    check_api_response(response, status_code=status_code)
