# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.core import create_role_rule
from kadi.lib.permissions.models import RoleRule
from kadi.lib.web import url_for
from kadi.modules.groups.models import GroupState
from tests.modules.utils import check_api_delete_subject_resource_role
from tests.utils import check_api_response


def test_remove_group_role_rule(client, db, user_session, dummy_group):
    """Test the internal "api.remove_group_role_rule" endpoint."""
    role_rule = create_role_rule("group", dummy_group.id, "member", "test", {})
    db.session.commit()

    with user_session():
        response = client.delete(
            url_for(
                "api.remove_group_role_rule",
                group_id=dummy_group.id,
                rule_id=role_rule.id,
            )
        )
        db.session.commit()

        check_api_response(response, status_code=204)
        assert not RoleRule.query.all()


def test_delete_group(api_client, dummy_group, dummy_personal_token):
    """Test the "api.delete_group" endpoint."""
    response = api_client(dummy_personal_token).delete(
        url_for("api.delete_group", id=dummy_group.id)
    )

    check_api_response(response, status_code=204)
    assert dummy_group.state == GroupState.DELETED


def test_remove_group_member(
    api_client, db, dummy_group, dummy_personal_token, dummy_user, new_user
):
    """Test the "api.remove_group_member" endpoint."""
    user = new_user()
    client = api_client(dummy_personal_token)
    endpoint = url_for(
        "api.remove_group_member", group_id=dummy_group.id, user_id=user.id
    )
    remove_creator_endpoint = url_for(
        "api.remove_group_member", group_id=dummy_group.id, user_id=dummy_user.id
    )

    check_api_delete_subject_resource_role(
        db,
        client,
        endpoint,
        user,
        dummy_group,
        remove_creator_endpoint=remove_creator_endpoint,
    )
