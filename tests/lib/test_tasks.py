# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.lib.tasks.models import Task
from kadi.lib.tasks.models import TaskState
from kadi.lib.tasks.utils import clean_tasks


def test_clean_tasks(monkeypatch, dummy_user):
    """Test if cleaning tasks works correctly."""
    pending_task = Task.create(creator=dummy_user, name="test", state=TaskState.PENDING)
    Task.create(creator=dummy_user, name="test", state=TaskState.SUCCESS)

    clean_tasks()

    assert Task.query.count() == 2

    monkeypatch.setitem(current_app.config, "FINISHED_TASKS_MAX_AGE", 0)

    clean_tasks()

    assert Task.query.one().id == pending_task.id
