# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from marshmallow import fields
from marshmallow import ValidationError
from werkzeug.exceptions import HTTPException

from kadi.lib.conversion import lower
from kadi.lib.conversion import strip
from kadi.lib.schemas import CustomPluck
from kadi.lib.schemas import CustomString
from kadi.lib.schemas import KadiSchema


@pytest.mark.parametrize(
    "data,is_valid", [(None, False), ({}, False), ({"test": "test"}, True)]
)
def test_kadi_schema(data, is_valid, request_context):
    """Test if the custom schema base class works correctly."""

    class _TestSchema(KadiSchema):
        test = fields.String(required=True)

    schema = _TestSchema()

    assert not schema._internal

    if is_valid:
        schema.load_or_400(data)
    else:
        with pytest.raises(HTTPException):
            schema.load_or_400(data)


@pytest.mark.parametrize(
    "data,allow_ws_only,filter,result",
    [
        (None, False, None, None),
        (" ", False, None, None),
        (" test ", False, None, " test "),
        (" test ", False, strip, "test"),
        (" Test ", False, [lower, strip], "test"),
        ("", True, None, ""),
        (" ", True, None, " "),
        (" ", True, strip, ""),
    ],
)
def test_custom_string(data, allow_ws_only, filter, result):
    """Test if the "CustomString" field works correctly."""
    field = CustomString(allow_ws_only=allow_ws_only, filter=filter)

    if result is not None:
        assert field.deserialize(data) == result
    else:
        with pytest.raises(ValidationError):
            CustomString().deserialize(data)


@pytest.mark.parametrize(
    "value,many,sort,result",
    [
        (None, False, False, None),
        ("a", False, False, "a"),
        (["b", "a", "c"], True, False, ["b", "a", "c"]),
        (["b", "a", "c"], True, True, ["a", "b", "c"]),
    ],
)
def test_custom_pluck_serialize(value, many, sort, result):
    """Test if the "CustomPluck" field works correctly when serializing."""

    class _TestSchema(KadiSchema):
        test = fields.String()

    field = CustomPluck(_TestSchema, "test", many=many, sort=sort)

    if many:
        data = [{"test": v} for v in value]
    else:
        data = {"test": value}

    assert field.serialize("test", {"test": data}) == result


@pytest.mark.parametrize(
    "value,many,sort,flatten,unique,result",
    [
        ("a", False, False, False, False, {"test": "a"}),
        ("a", False, False, True, False, "a"),
        (
            ["b", "a", "c", "a"],
            True,
            False,
            False,
            False,
            [{"test": "b"}, {"test": "a"}, {"test": "c"}, {"test": "a"}],
        ),
        (["b", "a", "c", "a"], True, True, True, True, ["a", "b", "c"]),
    ],
)
def test_custom_pluck_deserialize(value, many, sort, flatten, unique, result):
    """Test if the "CustomPluck" field works correctly when deserializing."""

    class _TestSchema(KadiSchema):
        test = fields.String()

    field = CustomPluck(
        _TestSchema, "test", many=many, sort=sort, flatten=flatten, unique=unique
    )

    if many:
        data = [{"test": v} for v in value]
    else:
        data = {"test": value}

    assert field.deserialize(value, "test", data) == result
