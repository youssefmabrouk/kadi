# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

import pytest
from PIL import Image

import kadi.lib.constants as const
from kadi.lib.api.models import PersonalToken
from kadi.lib.licenses.models import License
from kadi.lib.oauth.models import OAuth2ServerClient
from kadi.lib.oauth.models import OAuth2ServerToken
from kadi.lib.revisions.core import create_revision
from kadi.lib.security import hash_value
from kadi.lib.storage.local import create_default_local_storage
from kadi.modules.accounts.providers import LocalProvider
from kadi.modules.collections.core import create_collection
from kadi.modules.groups.core import create_group
from kadi.modules.records.core import create_record
from kadi.modules.records.models import File
from kadi.modules.records.models import FileState
from kadi.modules.records.models import TemporaryFile
from kadi.modules.records.models import TemporaryFileState
from kadi.modules.records.models import Upload
from kadi.modules.records.models import UploadType
from kadi.modules.templates.core import create_template
from kadi.modules.templates.models import TemplateType


@pytest.fixture
def new_user(db):
    """Fixture to create a factory for creating new local users."""
    count = 0

    def _new_user(
        username=None, password=None, email=None, displayname=None, system_role=None
    ):
        nonlocal count
        count += 1

        username = username if username is not None else f"user_{count}"
        password = password if password is not None else username
        email = email if email is not None else f"{username}@example.com"
        displayname = displayname if displayname is not None else username

        identity = LocalProvider.register(
            displayname=displayname,
            username=username,
            password=password,
            email=email,
            system_role=system_role,
        )
        return identity.user

    return _new_user


@pytest.fixture
def dummy_user(db):
    """Fixture to create a local dummy user."""
    username = password = "dummy-user"

    identity = LocalProvider.register(
        displayname="Dummy user",
        username=username,
        password=password,
        email=f"{username}@example.com",
    )
    return identity.user


@pytest.fixture
def new_record(db, dummy_user):
    """Fixture to create a factory for creating new records."""
    count = 0

    def _new_record(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        return create_record(
            creator=creator,
            identifier=f"record_{count}",
            title=f"Record {count}",
            **kwargs,
        )

    return _new_record


@pytest.fixture
def dummy_record(db, dummy_user):
    """Fixture to create a dummy record."""
    return create_record(
        creator=dummy_user, identifier="dummy-record", title="Dummy record"
    )


@pytest.fixture
def new_file(db, dummy_record, dummy_image, dummy_user):
    """Fixture to create a factory for creating new files."""
    count = 0

    def _new_file(
        creator=None, record=None, file_data=None, state=FileState.ACTIVE, **kwargs
    ):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user
        record = record if record is not None else dummy_record
        file_data = BytesIO(file_data) if file_data is not None else dummy_image

        buffer = file_data.getbuffer()

        file = File.create(
            creator=creator,
            record=record,
            name=f"dummy_{count}.jpg",
            size=len(buffer),
            checksum=hash_value(buffer, alg="md5"),
            state=state,
            **kwargs,
        )
        db.session.flush()

        storage = file.storage

        filepath = storage.create_filepath(str(file.id))
        storage.save(filepath, file_data)

        if file.magic_mimetype is None:
            file.magic_mimetype = storage.get_mimetype(filepath)

        create_revision(file, user=creator)

        db.session.commit()
        return file

    return _new_file


@pytest.fixture
def dummy_file(db, dummy_image, dummy_record, dummy_user):
    """Fixture to create a local dummy file.

    The content of the file consists of the content of the "dummy_image" fixture.
    """
    buffer = dummy_image.getbuffer()

    file = File.create(
        creator=dummy_user,
        record=dummy_record,
        name="dummy.jpg",
        size=len(buffer),
        checksum=hash_value(buffer, alg="md5"),
        mimetype=const.MIMETYPE_JPEG,
        magic_mimetype=const.MIMETYPE_JPEG,
        state=FileState.ACTIVE,
    )
    db.session.flush()

    storage = file.storage

    filepath = storage.create_filepath(str(file.id))
    storage.save(filepath, dummy_image)

    create_revision(file, user=dummy_user)

    db.session.commit()
    return file


@pytest.fixture
def new_upload(db, dummy_record, dummy_user):
    """Fixture to create a factory for creating new uploads."""
    count = 0

    def _new_upload(
        creator=None,
        record=None,
        size=0,
        upload_type=UploadType.CHUNKED,
        file=None,
        **kwargs,
    ):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user
        record = record if record is not None else dummy_record

        upload = Upload.create(
            creator=creator,
            record=record,
            size=size,
            file=file,
            upload_type=upload_type,
            name=f"dummy_{count}.txt",
            **kwargs,
        )

        db.session.commit()
        return upload

    return _new_upload


@pytest.fixture
def dummy_upload(db, dummy_record, dummy_user):
    """Fixture to create a chunked dummy upload."""
    upload = Upload.create(
        creator=dummy_user,
        record=dummy_record,
        size=0,
        name="dummy.txt",
        upload_type=UploadType.CHUNKED,
    )

    db.session.commit()
    return upload


@pytest.fixture
def dummy_temporary_file(db, dummy_image, dummy_record, dummy_user):
    """Fixture to create a dummy temporary file."""
    buffer = dummy_image.getbuffer()
    storage = create_default_local_storage()

    temporary_file = TemporaryFile.create(
        creator=dummy_user,
        record=dummy_record,
        name="dummy.jpg",
        size=len(buffer),
        state=TemporaryFileState.ACTIVE,
    )
    db.session.flush()

    filepath = storage.create_filepath(str(temporary_file.id))
    storage.save(filepath, dummy_image)

    db.session.commit()
    return temporary_file


@pytest.fixture
def new_collection(db, dummy_user):
    """Fixture to create a factory for creating new collections."""
    count = 0

    def _new_collection(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        return create_collection(
            creator=creator,
            identifier=f"collection_{count}",
            title=f"Collection {count}",
            **kwargs,
        )

    return _new_collection


@pytest.fixture
def dummy_collection(db, dummy_user):
    """Fixture to create a dummy collection."""
    return create_collection(
        creator=dummy_user, identifier="dummy-collection", title="Dummy collection"
    )


@pytest.fixture
def new_group(db, dummy_user):
    """Fixture to create a factory for creating new groups."""
    count = 0

    def _new_group(creator=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user

        return create_group(
            creator=creator,
            identifier=f"group_{count}",
            title=f"Group {count}",
            **kwargs,
        )

    return _new_group


@pytest.fixture
def dummy_group(db, dummy_user):
    """Fixture to create a dummy group."""
    return create_group(
        creator=dummy_user, identifier="dummy-group", title="Dummy group"
    )


@pytest.fixture
def new_template(db, dummy_user):
    """Fixture to create a factory for creating new templates."""
    count = 0

    def _new_template(creator=None, type=TemplateType.RECORD, data=None, **kwargs):
        nonlocal count
        count += 1

        creator = creator if creator is not None else dummy_user
        template_type = type

        if data is None:
            if template_type == TemplateType.RECORD:
                data = {}
            elif template_type == TemplateType.EXTRAS:
                data = []

        return create_template(
            creator=creator,
            type=template_type,
            data=data,
            identifier=f"template_{count}",
            title=f"Template {count}",
            **kwargs,
        )

    return _new_template


@pytest.fixture
def dummy_template(db, dummy_user):
    """Fixture to create a dummy record template."""
    return create_template(
        creator=dummy_user,
        type=TemplateType.RECORD,
        data={},
        identifier="dummy-template",
        title="Dummy template",
    )


@pytest.fixture
def new_personal_token(db, dummy_user):
    """Fixture to create a factory for creating new personal tokens.

    Note that instead of the personal token object, the actual token value will be
    returned.
    """

    def _new_personal_token(user=None, name=None, token=None, **kwargs):
        user = user if user is not None else dummy_user
        name = name if name is not None else "token"
        token = token if token is not None else PersonalToken.new_token()

        PersonalToken.create(user=user, name=name, token=token, **kwargs)

        db.session.commit()
        return token

    return _new_personal_token


@pytest.fixture
def dummy_personal_token(db, dummy_user):
    """Fixture to create a dummy personal token.

    Note that instead of the personal token object, the actual token value will be
    returned.
    """
    token = PersonalToken.new_token()
    PersonalToken.create(user=dummy_user, name="dummy-token", token=token)

    db.session.commit()
    return token


@pytest.fixture
def new_oauth2_server_client(db, dummy_user):
    """Fixture to create a factory for creating new OAuth2 server clients."""

    def _new_oauth2_server_client(user=None, redirect_uris=None, **kwargs):
        user = user if user is not None else dummy_user
        redirect_uris = (
            redirect_uris if redirect_uris is not None else ["https://foo.bar/callback"]
        )

        oauth2_server_client = OAuth2ServerClient.create(
            user=user,
            redirect_uris=redirect_uris,
            client_name="test",
            client_uri="https://foo.bar",
            **kwargs,
        )

        db.session.commit()
        return oauth2_server_client

    return _new_oauth2_server_client


@pytest.fixture
def dummy_oauth2_server_client(db, dummy_user):
    """Fixture to create a dummy OAuth2 server clients."""
    oauth2_server_client = OAuth2ServerClient.create(
        user=dummy_user,
        client_name="test",
        client_uri="https://foo.bar",
        redirect_uris=["https://foo.bar/callback"],
    )

    db.session.commit()
    return oauth2_server_client


@pytest.fixture
def new_oauth2_server_token(db, dummy_oauth2_server_client, dummy_user):
    """Fixture to create a factory for creating new OAuth2 server tokens."""

    def _new_oauth2_server_token(
        user=None, client=None, access_token=None, refresh_token=None, **kwargs
    ):
        user = user if user is not None else dummy_user
        client = client if client is not None else dummy_oauth2_server_client

        oauth2_server_token = OAuth2ServerToken.create(
            user=user,
            client=client,
            access_token=access_token,
            refresh_token=refresh_token,
            expires_in=100,
            **kwargs,
        )

        db.session.commit()
        return oauth2_server_token

    return _new_oauth2_server_token


@pytest.fixture
def dummy_license(db):
    """Fixture to create a dummy license."""
    license = License.create(name="test", title="test", url="https://example.org/test")

    db.session.commit()
    return license


@pytest.fixture
def dummy_image():
    """Fixture to create an in-memory JPEG image with a single white pixel."""
    image = Image.new("RGB", size=(1, 1), color=(255, 255, 255))

    image_data = BytesIO()
    image.save(image_data, format="JPEG")
    image_data.seek(0)

    return image_data
